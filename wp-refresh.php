<?php
// BACKUP ESSENTIAL FILES FIRST
/**
 * WP Essential Backup
 * WordPress Backup Tool
 * (c) 2015 Topher Tebow  - work for hire?
 * Developed for Sitelock Inc
 **/
 /*
 6.8.2016, changed the replace button to either dynamically create a backup or delete one.
 6.2.2016, changed the layour slight, added the replace and remove buttons.
 */
 
 
 // Turn off error reporting
ini_set('display_errors','On');

function backUp(){

	$adm = 'wp-admin';
	$inc = 'wp-includes';
	$archive = 'core_files_bk_'. date("m.d.Y").'.zip';

	class FlxZipArchive extends ZipArchive {

	public function addDir($location, $name) {
		$this->addEmptyDir($name);

		$this->addDirDo($location, $name);
	}

// Add files and directories to the archive or fail
	private function addDirDo($location, $name) {
		$location .= '/';
		$name .= '/';

		$dir = opendir ($location);
		while ($file = readdir($dir))
			{
				if ($file == '.' || $file == '..') continue;
				$do = (filetype( $location . $file) == 'dir') ? 'addDir' : 'addFile';
				$this->$do($location . $file, $name . $file);
			}
		}
	}

	$za = new FlxZipArchive;
	$res = $za->open($archive, ZipArchive::CREATE);
	if($res === TRUE) 
	{
		$za->addDir($adm, basename($adm));
		$za->addDir($inc, basename($inc));
		$za->addFile('wp-config.php', 'wp-config.php');
		$za->close();
	}
	else  { echo 'Could not create a zip archive';}

//Download the archive
}

// REINSTALL CORE WORDPRESS FILES
/**
 * WP Refresh
 * Live WordPress Re-Imaging Tool
 * (c) 2015 Morgan Breden  - work for hire?
 * Developed with love for Sitelock Inc
 **/
class WpRefresh {
	/**
	 * @var String|Null version
	 *	Holds the detected (or manually set) version of WordPress
	**/
	private $version = NULL;
	/**
	 * @var String wp_download_url
	 * Holds the URL prefix WordPress Archives are downloaded from.
	**/
	private $wp_download_url = 'http://wordpress.org/wordpress-';
	/**
	 * @var String archive_type
	 * File extension of the archive type to download from WordPress.org
	**/
	private $archive_type = 'zip';
	/**
	 *	@var Array errors
	 * Holds errors during the script run
	 **/
	private $errors = array();
	/**
	 *	@var Array notice
	 * Holds notice during the script run
	 **/
	private $notice = array();
	/**
	 *	default folder to run scripts
	 **/
	private $defFolder = 'sitelockWordpressBackup';
	
	public function __construct() {
		if(isset($_POST['version'])) {
			$this->setVersion($_POST['version']);
		}
		if(isset($_POST['go'])) {
			$this->init();
		}
		if(isset($_POST['unlink'])){
			unlink(__FILE__);
		}
		if(isset($_POST['go2'])){
			//do stuff.
			$this->init2();
		}
	}
	/**
	 * @method init
	 * Main call function. This is where the magic happens.
	**/
	public function init() {
		if($this->version !== NULL) {
			try {
				$this->downloadArchive();
			} catch (Exception $e) {
				$this->handleException($e);
			}
			
			try {
				$this->unzip();
			} catch (Exception $e) {
				$this->handleException($e);
			}
			try {
				$this->overwrite(getcwd().'/'.$this->defFolder.'/'.'wordpress', getcwd());//TODO: now pulling from defined extraction folder
			} catch (Exception $e) {
				$this->handleException($e);
			}
			//nuke the folder we extracted to.
			$this->delDir(getcwd().'/'.$this->defFolder.'/');//NOTE: this REQUIRES php 5.2+.

		}
		
	}
	public function init2() {
		if(file_exists('./wp-admin.bak'))
		{
			$this->delDir(getcwd().'/wp-admin.bak/');
			$this->delDir(getcwd().'/wp-includes.bak/');
			$this->addNotice('removed the backup folders');
		}
		else
		{
			if($this->version !== NULL) {
				try {
					$this->downloadArchive();
				} catch (Exception $e) {
					$this->handleException($e);
				}
				
				try {
					$this->unzip();
				} catch (Exception $e) {
					$this->handleException($e);
				}
				/*
				//TODO: why are we overwriting?  this will not remove bad files, just fix broken ones.
				try {
					$this->overwrite(getcwd().'/'.$this->defFolder.'/'.'wordpress', getcwd());//TODO: now pulling from defined extraction folder
				} catch (Exception $e) {
					$this->handleException($e);
				}*/
				try {
					$this->renameWpFiles();
				} catch (Exception $e) {
					$this->handleException($e);
				}
				//nuke the folder we extracted to.
				$this->delDir(getcwd().'/'.$this->defFolder.'/');//NOTE: this REQUIRES php 5.2+. 

			}
			else
			{
				echo 'version set to null... wtf';
			}
		}
	}
	private function renameWpFiles()
	{
		if(rename('./wp-admin', './wp-admin.bak' )&&rename('./wp-includes', './wp-includes.bak' ))
		{
			$this->addNotice('wp-core folders renamed.');
		}
		else
		{
			throw new Exception('renaming of WordPress files failed');
		}
		$base = './'. $this->defFolder.'/wordpress';
		if(rename($base.'/wp-admin','./wp-admin')&&rename($base.'/wp-includes','./wp-includes' ))
		{
			$this->addNotice('wp-core folders imported.');
		}
		else
		{
			throw new Exception('importing of WordPress files failed ... tried to import: '.$base.'/wp-admin');
		}
		
	}
	/**
	 * @method setVersion
	 * Public setter for version veriable
	 * 
	 * @var String version
	 * The version to set in-class
	**/
	public function setVersion($version) {
		$this->version = $version;
	}
	/**
	 * @method getVersion
	 * Public getter for version veriable
	 * 
	 * @return String version
	**/
	public function getVersion() {
		return $this->version;
	}
	/**
	 * @method downloadArchive
	 * Downloads the WordPress archive of the version requested to the local server.
	 * @throws Exception
	**/
	private function downloadArchive() {
		echo 'test';
		$download_url = $this->wp_download_url . $this->version . '.' . $this->archive_type;
		if(!copy($download_url, $this->version . '.' . $this->archive_type)) {
			throw new Exception('Failed to copy WordPress.org archive to local host.');
		}
	}
	/**
	 * @method unzip
	 * Unzips the downloaded WordPress archive into CWD/wordpress/
	 * @throws Exception
	**/
	private function unzip() {
		$zip = new ZipArchive();
		if($zip->open($this->version . '.' . $this->archive_type)) {
			$zip->extractTo(getcwd().'/'.$this->defFolder.'/');//TODO: now extracting to defFolder to prevent overwriting existing sites
			$zip->close();
		} else {
			throw new Exception('Failed to expand zip archive.');
		}
	}
	/**
	 * @method overwrite
	 * Takes the unzipped WordPress files and moves them to the installation DIR
	 **/
	private function overwrite($source, $destination) {  //TODO: figure out why this fails at life.
	
		$source = $source . '/';
		$destination = $destination . '/';
		//if($source != 'wordpress/') {
		//	$destination = preg_replace('/^\.\//', '', $destination);
		//}
		// Get array of all source files
		$files = scandir($source);
		// Cycle through all source files
		foreach ($files as $file) {
		  if(in_array($file, array(".",".."))) continue;
		  if(is_dir($source . $file)) {//
		  	$this->overwrite($source . $file ,$destination.$file);//recursion is recursion is
		  	continue;
		  }
		  // If we copied this successfully, mark it for deletion
		  if(!copy($source.$file, $destination.$file)) {
		  	$e = new Exception('Failed to overwrite core file.'. $destination.$file."<br />");
			$this->handleException($e);
		  } else {
		  	$this->addNotice("Copied - \"" . $source . $file . "\" TO \"" . $destination.$file . "\"<br />");
		  }
		}
	}
	/**
	 * @method errors
	 * Displays errors encountered during the run.
	**/
	public function errors() {
		$result = "";
		foreach($this->errors as $err) {
			$result .= $err . "\n";
		}
		return $result;
	}
	public function delDir($dir)
	{
		
		$it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
		$files = new RecursiveIteratorIterator($it,
					 RecursiveIteratorIterator::CHILD_FIRST);
		foreach($files as $file) {
			if ($file->isDir()){
				rmdir($file->getRealPath());
			} else {
				unlink($file->getRealPath());
			}
		}
		rmdir($dir);
	}
	/**
	 * @method notices
	 * Displays notices encountered during the run.
	**/
	public function notices() {
		$result = "";
		foreach($this->notice as $nt) {
			$result .= $nt . "\n";
		}
		return $result;
	}
	/**
	 * @method addError
	 * Adds an error to the error array
	 *
	 * @var String $error
	**/
	private function addError($error) {
		$this->errors[] = $error;
	}
	/**
	 * @method addNotice
	 * Adds an notice to the notice array
	 *
	 * @var String $notice
	**/
	private function addNotice($notice) {
		$this->notice[] = $notice;
	}
	/**
	 * @method handleException
	 * Prints exceptions to the screen and kills the script.
	**/
	private function handleException(Exception $e) {
		$this->addError($e->getMessage());
	}
}

//Main execution path
	
$refresh = New WpRefresh();

if (is_file('wp-includes/version.php'))
{
    require_once('wp-includes/version.php');
		$refresh->setVersion($wp_version); //should be pulling file contents, not an include.
}
else
{
	echo("no wp-includes/version.php file, assuming 4.5.2");
	$refresh->setVersion('4.5.2');
}

if(isset($_GET['action'])=='backUp') {
    backUp();
}else{}
//show form
?>
<html lang="en">
<head>
	<title>WP Refresh</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
</head>
<body>
<div class="container-fluid">
	<div class="col-md-6">
		<form action="wp-refresh.php" method="POST">
			<div class="form-group">
			<input type="hidden" name="versioninfo" value="<?php echo $refresh->getVersion(); ?>">
			</div>
		
			<div class="form-group">
				<label for="version">WordPress Version <?php 
				if($refresh->getVersion() !== NULL) 
				{
					echo '<span class="text-info">Auto-detected from wp-includes/version.php</span>';
				}
				else
				{
					echo '<span class="text-info">WordPress version not detected!  Proceed with caution.</span>';
				}				
					?></label>
				<input type="text" class="form-control" id="version" name="version" value="<?php echo $refresh->getVersion(); ?>" placeholder="0.0.0" />
				</div>
					<p class="bg-warning">
	Remove core WordPress folders and import good files, folders are renamed wp-admin.bak and wp-includes.bak.
	</p>
			<div class="form-group">
				
				<button type="submit" name="go2" value="1" class="btn btn-success btn-lg btn-block">
				<?php 
				if(file_exists('./wp-admin.bak'))
				{
					echo 'remove backups';
				}
				else
				{
					echo 'backup and replace core files';
				}
				?>
				</button>
				<button type="submit" name="unlink" value="1" class="btn btn-success btn-lg btn-block">Remove</button>
			</div>
		</form>
		<p class="bg-danger">
			<?php echo $refresh->errors(); ?>
		</p>
		<p class="bg-success">
			<?php echo $refresh->notices(); ?>
		</p>
	</div>
</div>
</body>
</html>
