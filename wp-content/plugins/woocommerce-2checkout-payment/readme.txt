=== 2Checkout Payment Gateway for WooCommerce ===
Contributors: nmedia
Tags: payment gateway, 2co payment gateway, 2checkout woocommerce payment, woocommerce payment gateway
Donate link: http://www.najeebmedia.com/donate
Requires at least: 3.5
Tested up to: 4.9
Stable tag: 2.3
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

It's a WooCommerce extension allow clients make payment with 2Checkout.

== Description ==
WooCommerce payment gateway plugin for 2Checkout which accept payments from client. Fully supported with WordPress and WooCommerce latest versions. 2Checkout latest API 2.0 compatible

= Features =
* Itemized Checkout - will display each item with SKU/ID
* Pass all billing and shipping data to 2CO purchase page
* Enable/Disable Test Mode

= 2Checkout Pro Features =
* Credit Card Form on Site Payment
* PayPal Direct Checkout
* Skipp Billing and Shipping Section
* Currency Conversion for Non-supported currencies with live rates
* [More detail About Pro Versoin](https://najeebmedia.com/wordpress-plugin/woocommerce-2checkout-payment-gateway-with-inline-support/)

= Quick Video - PayPal Direct =
[vimeo https://vimeo.com/283880739]

= How to Setup Account = 
[Step by step 2Checkout Account Setup Guide](https://najeebmedia.com/2018/08/08/woocommerce-2checkout-payment-gateway-setup-guide/)

== Installation ==
1. Upload plugin directory to the `/wp-content/plugins/` directory
2. Activate the plugin through the `Plugins` menu in WordPress
3. After activation, you can set options from `WooCommerce -> Settings -> Checkout` menu

== Frequently Asked Questions ==
= How to setup my 2Checkout Account? =
[Step by step 2Checkout Account Setup Guide](https://najeebmedia.com/2018/08/08/woocommerce-2checkout-payment-gateway-setup-guide/)

= How to found my Seller/Account Number? =
[Step by step 2Checkout Account Setup Guide](https://najeebmedia.com/2018/08/08/woocommerce-2checkout-payment-gateway-setup-guide/)

= How set page redirect =
[Step by step 2Checkout Account Setup Guide](https://najeebmedia.com/2018/08/08/woocommerce-2checkout-payment-gateway-setup-guide/)

= I am new to 2Checkout, can I have some quick overview? =
[Step by step 2Checkout Account Setup Guide](https://najeebmedia.com/2018/08/08/woocommerce-2checkout-payment-gateway-setup-guide/)


== Screenshots ==
1. 2Checkout Settings
2. WooCommerce Checkout Page
3. Itemized checkout
4. Accepting Credit Cart on Site (PRO)
4. Skipping Billing and Shipping Section on 2Checkout (PRO)

== Changelog ==
= 2.3 September 27, 2018 =
* Bug fixed: [Shipping issue fixed](https://wordpress.org/support/topic/no-available-shipping-methods-5/)
* Bug fixed: Sometime Hash Mismatch, it's also fixed
= 2.2 August, 2018 =
* Feature: WooCommerce latest version compatible
* Feature: Add 2Checkout Order Number in Order Notes.

= 2.1 April 3, 2018 =
Bug fixed: [Zip/Postal Code and Country were not updated on checkout, now it's fixed](https://wordpress.org/support/topic/zip-code-3/)
= 2.0 =
* WooCommerce 3.0 Compatible
* Itemized Billing (Products, Tax, Shipping, Fees)
= 1.7 =
* Currency code passed to checkout
= 1.6 =
* BUG Fixed: Variable products prices were not correct, now it's fixed
* Set Product as Tangible or Intangible
* Sending Product ID to Cart Data
= 1.5 =
* fixed return url issue
= 1.4 =
* get_shipping() function is replaced with get_total_shipping
= 1.3 =
* Hash Mismatch issue fixed when client redirected to shop from 2checkout payment
= 1.2 =
* Now Secret word support added in plugin
* clear the cart once order is verified.
= 1.1 =
* fix callback url issue when payment is made.
* some labels are updated.
= 1.0 =
* It's first release











== Upgrade Notice ==
Nothing