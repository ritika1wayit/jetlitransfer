<?php

// [google_map]
function shortcode_google_map($atts, $content=null, $code) {
	
    $randomid = rand();
	
    extract(shortcode_atts(array(
		'lat'  => '42.4515577',
        'long' => '18.5336152',
        'height' => '400px',
		'color' => '#6990cb',
		'zoom' => '15',
		'control_elements' => 'enabled',
	), $atts));

	ob_start();
    
	?>    
	
	<script type="text/javascript">
    
    function initialize() {
        var styles = {
            'wstock':  [{
            "featureType": "administrative",
            "stylers": [
              { "visibility": "on" }
            ]
          },
          {
            "featureType": "road",
            "stylers": [
              { "visibility": "on" },
              { "hue": "<?php echo esc_html($color) ?>" }
            ]
          },
          {
            "stylers": [
			  { "visibility": "on" },
			  { "hue": "<?php echo esc_html($color) ?>" },
			  { "saturation": -50 }
            ]
          }
        ]};
        
        var myLatlng = new google.maps.LatLng(<?php echo esc_html($lat) ?>, <?php echo esc_html($long) ?>);
        var myOptions = {
            zoom: <?php echo esc_html($zoom) ?>,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            //disableDefaultUI: true,
            mapTypeId: 'wstock',
            draggable: true,
            <?php if ($control_elements == "disabled") : ?>
				zoomControl: false,
				panControl: false,
				mapTypeControl: false,
				scaleControl: false,
				streetViewControl: false,
				overviewMapControl: false,
				draggable: false,
			<?php endif; ?>
            scrollwheel: false,
            //disableDoubleClickZoom: false
        }
        var map = new google.maps.Map(document.getElementById("map_canvas_<?php echo esc_html($randomid); ?>"), myOptions);
        var styledMapType = new google.maps.StyledMapType(styles['wstock'], {name: 'wstock'});
        map.mapTypes.set('wstock', styledMapType);
        
        var marker = new google.maps.Marker({
            position: myLatlng, 
            map: map,
            title:""
        });   
    }
    
    google.maps.event.addDomListener(window, 'load', initialize);
    google.maps.event.addDomListener(window, 'resize', initialize);
    
    </script>
    
    <div id="map_container">
        <div id="map_canvas_<?php echo esc_html($randomid); ?>" style="height:<?php echo esc_html($height) ?>;"></div>
    </div>

	<?php
	wp_reset_query();
	$content = ob_get_contents();
	ob_end_clean();
	return $content;
}

add_shortcode("google_map", "shortcode_google_map");