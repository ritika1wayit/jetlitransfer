<?php
/**
* Plugin Name: PBD Jetlitransfer Agent
* Plugin URI: http://producedbydeuce.com/
* Description: PBD Custom plugin for jetlitransfer.com.
* Version: 1.0
* Author: PBDDevTeam
* Author URI: http://producedbydeuce.com/
* License: A "Slug" license name e.g. GPL12
*/

/*
add_filter( 'woocommerce_checkout_fields' , 'default_values_checkout_fields' );
function default_values_checkout_fields( $fields ) {
	$is_agent = false;
	if(is_user_logged_in()) {
		//var_dump(get_userdata(get_current_user_id()));
		if(current_user_can('agent')) {
			$is_agent = true;
		}
	}

	if($is_agent==true) {
		$fields['billing']['billing_is_agent']['default'] = '1';
	}

	$fields['billing']['billing_is_agent']['default'] = '0';
	return $fields;
}
*/

function pbd_woo_agent_list_shortcode( $atts) {
	// security feature
	if(!is_user_logged_in()) {
		wp_safe_redirect('/my-account/');
	}
	
	$is_agent = false;
	if(is_user_logged_in()) {
		//var_dump(get_userdata(get_current_user_id()));
		if(current_user_can('agent')) {
			$is_agent = true;
		}
	}

	if($is_agent==true) {
		if(isset($_GET['pbd_search_order_id']) && strlen($_GET['pbd_search_order_id'])>0) {
			$post_id = (int)trim($_GET['pbd_search_order_id']);
			//echo 'search using POST_ID '.$post_id;
			$customer_orders = get_posts( apply_filters( 'woocommerce_my_account_my_orders_query', array(
				'numberposts' => 100,
				'meta_key'    => '_customer_user',
				'meta_value'  => get_current_user_id(),
				'post__in'    => array($post_id),
				'post_type'   => wc_get_order_types( 'view-orders' ),
				'post_status' => array_keys( wc_get_order_statuses() )
			) ) );

		} elseif(isset($_GET['pbd_search_name']) && strlen($_GET['pbd_search_name']) > 0) {
			$name = ucfirst(strtolower(trim($_GET['pbd_search_name'])));
			//echo 'Search using name '.$name;
			$customer_orders = get_posts( apply_filters( 'woocommerce_my_account_my_orders_query', array(
				'numberposts' => 100,
				'meta_query'  => array( 
									array( 
									'key' => '_customer_user', 
									'value' => get_current_user_id(),
									), array(
									'key' => '_billing_first_name', 
									'value' => $name,
									'compare' => 'LIKE'
									),
								),
				'post_type'   => wc_get_order_types( 'view-orders' ),
				'post_status' => array_keys( wc_get_order_statuses() )
			) ) );
			//var_dump($customer_orders);
		} 
		 elseif(isset($_GET['pbd_search_phone_from']) && strlen($_GET['pbd_search_phone_from']) > 0) {
			$phoneFrom = ucfirst(strtolower(trim($_GET['pbd_search_phone_from'])));
			//echo 'Search using name '.$name;
			$customer_orders = get_posts( apply_filters( 'woocommerce_my_account_my_orders_query', array(
				'numberposts' => 100,
				'meta_query'  => array( 
									array( 
									'key' => '_customer_user', 
									'value' => get_current_user_id(),
									), array(
									'key' => '_billing_phone', 
									'value' => $phoneFrom,
									'compare' => 'LIKE'
									),
								),
				'post_type'   => wc_get_order_types( 'view-orders' ),
				'post_status' => array_keys( wc_get_order_statuses() )
			) ) );
			//var_dump($customer_orders);
		}
		elseif(isset($_GET['pbd_search_phone_to']) && strlen($_GET['pbd_search_phone_to']) > 0) {
			$phoneTo = ucfirst(strtolower(trim($_GET['pbd_search_phone_to'])));
			//echo 'Search using name '.$name;
			$customer_orders = get_posts( apply_filters( 'woocommerce_my_account_my_orders_query', array(
				'numberposts' => 100,
				'meta_query'  => array( 
									array( 
									'key' => '_customer_user', 
									'value' => get_current_user_id(),
									), array(
									'key' => '_shipping_phone', 
									'value' => $phoneTo,
									'compare' => 'LIKE'
									),
								),
				'post_type'   => wc_get_order_types( 'view-orders' ),
				'post_status' => array_keys( wc_get_order_statuses() )
			) ) );
			//var_dump($customer_orders);
		}
		else {
			$customer_orders = get_posts( apply_filters( 'woocommerce_my_account_my_orders_query', array(
				'numberposts' => 100,
				'meta_key'    => '_customer_user',
				'meta_value'  => get_current_user_id(),
				'post_type'   => wc_get_order_types( 'view-orders' ),
				'post_status' => array_keys( wc_get_order_statuses() )
			) ) );			
		}		
		

		
		$arr = array();
		
		foreach ( $customer_orders as $customer_order ) {
			$order      = wc_get_order( $customer_order );
			$name_from  = trim(trim($order->get_billing_first_name()).' '.trim($order->get_billing_last_name()));
			$name_to    = trim(trim($order->get_shipping_first_name()).' '.trim($order->get_shipping_last_name()));
			$phone_from = trim($order->get_billing_phone());
			$phone_to   = trim(get_post_meta($order->id, 'shipping_phone', true));
			$arr[$phone_from][$phone_to]['order_id']   = $order->id;
			$arr[$phone_from][$phone_to]['name_from']  = $name_from;
			$arr[$phone_from][$phone_to]['name_to']    = $name_to;
			$arr[$phone_from][$phone_to]['phone_from'] = $phone_from;
			$arr[$phone_from][$phone_to]['phone_to']   = $phone_to;
			$arr[$phone_from][$phone_to]['address_to'] = $order->get_formatted_shipping_address();
		}
	//var_dump($arr);
		if($arr) {
?>
<div class="woocommerce">
	<div class="row">
		<div class="large-9 large-centered columns">
			<div class="my_account_container">
				<div class="myaccount_user">
					<div class="myaccount_user_inner">
					</div>
				</div>
				<div class="my_account_inside bordered">  
					<h2>Agent Customer List</h2>
						<form method="get">
							<input type="text" name="pbd_search_order_id" value="<?php echo $post_id; ?>" placeholder="order ID"/>
							<input type="text" name="pbd_search_name" value="<?php echo $name; ?>" placeholder="First Name"/>
							<input type="text" name="pbd_search_phone_from" value="<?php echo $phoneFrom; ?>" placeholder="Billing Phone"/>
							<input type="text" name="pbd_search_phone_to" value="<?php echo $phoneTo; ?>" placeholder="Shipping Phone"/>
							<input type="submit" class="button" name="pbd-search-my-account-order-id" value="<?php esc_attr_e( 'Search', 'woocommerce' ); ?>" />
						</form>
						<table class="shop_table shop_table_responsive my_account_orders">
							<thead>
								<tr>
									<th class="order-number"><span class="nobr">Customer</span></th>
									<th class="order-date"><span class="nobr">Recipient</span></th>
									<th class="order-actions"><span class="nobr">&nbsp;</span></th>
								</tr>
							</thead>
							<tbody>
							<?php foreach ( $arr as $key1 => $val1 ) : ?>
								<?php foreach ( $val1 as $key2 => $val2 ) : ?>
								<tr class="order">
									<td class="order-from" data-title="From">
										<?php echo $val2['name_from']; ?><br />
										Ph: <?php echo $val2['phone_from']; ?>
									</td>
									<td class="order-to" data-title="To">
										<?php echo $val2['address_to']; ?><br />
										Ph: <?php echo $val2['phone_to']; ?><br />
									</td>
									<td class="order-actions" data-title="&nbsp;">
										<a href="/agent-carbon-form/?order=<?php echo $val2['order_id']; ?>" class="button pbd-checkout-with-this">Checkout with this</a>
									</td>
								</tr>
								<?php endforeach; ?>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div><!-- .my_account_container-->
		</div><!-- .large-8-->
	</div>
</div>
<?php
		}
	}
}
add_shortcode( 'pbd_woo_agent_list', 'pbd_woo_agent_list_shortcode' );

class Pbd_My_Account_Edit_Order_Billing_Address_Endpoint {
	/**
	 * Custom endpoint name.
	 *
	 * @var string
	 */
	public static $endpoint = 'edit-order-billing-address';
	/**
	 * Plugin actions.
	 */
	public function __construct() {
		// Actions used to insert a new endpoint in the WordPress.
		add_action( 'init', array( $this, 'add_endpoints' ) );
		add_filter( 'query_vars', array( $this, 'add_query_vars' ), 0 );
		// Change the My Accout page title.
		add_filter( 'the_title', array( $this, 'endpoint_title' ) );
		// Inserting your new tab/page into the My Account page.
		add_filter( 'woocommerce_account_menu_items', array( $this, 'new_menu_items' ) );
		add_action( 'woocommerce_account_' . self::$endpoint .  '_endpoint', array( $this, 'endpoint_content' ) );
	}
	/**
	 * Register new endpoint to use inside My Account page.
	 *
	 * @see https://developer.wordpress.org/reference/functions/add_rewrite_endpoint/
	 */
	public function add_endpoints() {
		add_rewrite_endpoint( self::$endpoint, EP_ROOT | EP_PAGES );
		add_action( 'template_redirect', array( __CLASS__, 'save_order_billing_address' ) );
	}
	/**
	 * Add new query var.
	 *
	 * @param array $vars
	 * @return array
	 */
	public function add_query_vars( $vars ) {
		$vars[] = self::$endpoint;
		$vars[] = 'edit_order_billing_address';
		$vars[] = 'pbd_order_id';
		return $vars;
	}
	/**
	 * Set endpoint title.
	 *
	 * @param string $title
	 * @return string
	 */
	public function endpoint_title( $title ) {
		global $wp_query;
		$is_endpoint = isset( $wp_query->query_vars[ self::$endpoint ] );
		if ( $is_endpoint && ! is_admin() && is_main_query() && in_the_loop() && is_account_page() ) {
			// New page title.
			$title = __( 'Edit Order Billing Address', 'woocommerce' );
			remove_filter( 'the_title', array( $this, 'endpoint_title' ) );
		}
		return $title;
	}

	/**
	 * Save and and update a order billing  address
	 */
	public static function save_order_billing_address() {
		global $wp;
		
		if ( 'POST' !== strtoupper( $_SERVER['REQUEST_METHOD'] ) ) {
			return;
		}

		if ( empty( $_POST['action'] ) || 'edit_order_billing_address' !== $_POST['action'] || empty( $_POST['_wpnonce'] ) || ! wp_verify_nonce( $_POST['_wpnonce'], 'woocommerce-edit_order_billing_address' ) ) {
			return;
		}
		//var_dump($_POST);
		$order_id = $wp->query_vars['pbd_order_id'];
		//var_dump('run save_order_billing_address '.$order_id); die();
		wc_nocache_headers();

		$user_id = get_current_user_id();

		if ( $user_id <= 0 ) {
			return;
		}

		$load_address = 'billing';

		$address = WC()->countries->get_address_fields( esc_attr( $_POST[ $load_address . '_country' ] ), $load_address . '_' );

		foreach ( $address as $key => $field ) {

			if ( ! isset( $field['type'] ) ) {
				$field['type'] = 'text';
			}

			// Get Value.
			switch ( $field['type'] ) {
				case 'checkbox' :
					$_POST[ $key ] = (int) isset( $_POST[ $key ] );
					break;
				default :
					$_POST[ $key ] = isset( $_POST[ $key ] ) ? wc_clean( $_POST[ $key ] ) : '';
					break;
			}

			// Hook to allow modification of value.
			$_POST[ $key ] = apply_filters( 'woocommerce_process_myaccount_field_' . $key, $_POST[ $key ] );

			// Validation: Required fields.
			if ( ! empty( $field['required'] ) && empty( $_POST[ $key ] ) ) {
				wc_add_notice( sprintf( __( '%s is a required field.', 'woocommerce' ), $field['label'] ), 'error' );
			}

			if ( ! empty( $_POST[ $key ] ) ) {

				// Validation rules.
				if ( ! empty( $field['validate'] ) && is_array( $field['validate'] ) ) {
					foreach ( $field['validate'] as $rule ) {
						switch ( $rule ) {
							case 'postcode' :
								$_POST[ $key ] = strtoupper( str_replace( ' ', '', $_POST[ $key ] ) );
								if ( ! WC_Validation::is_postcode( $_POST[ $key ], $_POST[ $load_address . '_country' ] ) ) {
									wc_add_notice( __( 'Please enter a valid postcode / ZIP.', 'woocommerce' ), 'error' );
								} else {
									$_POST[ $key ] = wc_format_postcode( $_POST[ $key ], $_POST[ $load_address . '_country' ] );
								}
								break;
							case 'phone' :
								$_POST[ $key ] = wc_format_phone_number( $_POST[ $key ] );
								if ( ! WC_Validation::is_phone( $_POST[ $key ] ) ) {
									wc_add_notice( sprintf( __( '%s is not a valid phone number.', 'woocommerce' ), '<strong>' . $field['label'] . '</strong>' ), 'error' );
								}
								break;
							case 'email' :
								$_POST[ $key ] = strtolower( $_POST[ $key ] );
								if ( ! is_email( $_POST[ $key ] ) ) {
									wc_add_notice( sprintf( __( '%s is not a valid email address.', 'woocommerce' ), '<strong>' . $field['label'] . '</strong>' ), 'error' );
								}
								break;
						}
					}
				}
			}
		}

		do_action( 'woocommerce_after_save_address_validation', $user_id, $load_address, $address );

		if ( 0 === wc_notice_count( 'error' ) ) {
/*
			$customer = new WC_Customer( $user_id );

			if ( $customer ) {
				foreach ( $address as $key => $field ) {
					if ( is_callable( array( $customer, "set_$key" ) ) ) {
						$customer->{"set_$key"}( wc_clean( $_POST[ $key ] ) );
					} else {
						$customer->update_meta_data( $key, wc_clean( $_POST[ $key ] ) );
					}

					if ( WC()->customer && is_callable( array( WC()->customer, "set_$key" ) ) ) {
						WC()->customer->{"set_$key"}( wc_clean( $_POST[ $key ] ) );
					}
				}
				$customer->save();
			}
*/
			$order = wc_get_order( $order_id );
			if($order) {
				if(isset($_POST['billing_first_name']) && strlen($_POST['billing_first_name'])>1) { $order->set_billing_first_name($_POST['billing_first_name']); }
				if(isset($_POST['billing_last_name']) && strlen($_POST['billing_last_name'])>1) { $order->set_billing_last_name($_POST['billing_last_name']); }
				if(isset($_POST['billing_company']) && strlen($_POST['billing_company'])>1) { $order->set_billing_company($_POST['billing_company']); }
				if(isset($_POST['billing_address_1']) && strlen($_POST['billing_address_1'])>1) { $order->set_billing_address_1($_POST['billing_address_1']); }
				if(isset($_POST['billing_address_2']) && strlen($_POST['billing_address_2'])>1) { $order->set_billing_address_2($_POST['billing_address_2']); }
				if(isset($_POST['billing_city']) && strlen($_POST['billing_city'])>1) { $order->set_billing_city($_POST['billing_city']); }
				if(isset($_POST['billing_state']) && strlen($_POST['billing_state'])>1) { $order->set_billing_state($_POST['billing_state']); }
				if(isset($_POST['billing_postcode']) && strlen($_POST['billing_postcode'])>1) { $order->set_billing_postcode($_POST['billing_postcode']); }
				if(isset($_POST['billing_country']) && strlen($_POST['billing_country'])>1) { $order->set_billing_country($_POST['billing_country']); }
				if(isset($_POST['billing_email']) && strlen($_POST['billing_email'])>1) { $order->set_billing_email($_POST['billing_email']); }
				if(isset($_POST['billing_phone']) && strlen($_POST['billing_phone'])>1) { $order->set_billing_phone($_POST['billing_phone']); }
				$order->save();
			}
			wc_add_notice( __( 'Order Billing Address changed successfully.', 'woocommerce' ) );

			do_action( 'woocommerce_customer_save_order_billing_address', $user_id, $load_address );

			wp_safe_redirect( wc_get_endpoint_url( 'view-order', $order_id, wc_get_page_permalink( 'myaccount' ) ) );
			exit;
		}
	}




	/**
	 * Insert the new endpoint into the My Account menu.
	 *
	 * @param array $items
	 * @return array
	 */
	public function new_menu_items( $items ) {
		// Remove the logout menu item.
		$logout = $items['customer-logout'];
		unset( $items['customer-logout'] );
		// Insert your custom endpoint.
		$items[ self::$endpoint ] = __( 'Edit Order Billing Address', 'woocommerce' );
		// Insert back the logout item.
		$items['customer-logout'] = $logout;
		return $items;
	}
	/**
	 * Endpoint HTML content.
	 */
	public function endpoint_content($var) {
		if($var) {
			$order = wc_get_order( $var );
			global $woocommerce, $current_user;
			$page_title = esc_html__( 'Billing Address', 'woocommerce' );
			$current_user = wp_get_current_user();
			$load_address = 'billing';
			$address = WC()->countries->get_address_fields( get_user_meta( get_current_user_id(), 'billing_country', true ), 'billing_' );
			$addressx = $order->get_address( 'billing' );

			// Enqueue scripts
			wp_enqueue_script( 'wc-country-select' );
			wp_enqueue_script( 'wc-address-i18n' );

			// Prepare values
			foreach ( $address as $key => $field ) {
				$value = $addressx[str_replace('billing_','',$key)];
				if ( ! $value ) {
					switch ( $key ) {
						case 'billing_email' :
						case 'shipping_email' :
							$value = $current_user->user_email;
						break;
						case 'billing_country' :
						case 'shipping_country' :
							$value = WC()->countries->get_base_country();
						break;
						case 'billing_state' :
						case 'shipping_state' :
							$value = WC()->countries->get_base_state();
						break;
					}
				}
				$address[ $key ]['value'] = $value;
			}
		 ?>
		<style>
		.site_header.with_featured_img,
		.site_header.without_featured_img {
		margin-bottom: 50px;
		}
		</style>

		<div class="row">
			<div class="large-9 large-centered columns">
				<form method="post">
					<h3><?php echo apply_filters( 'woocommerce_my_account_edit_address_title', $page_title, $load_address ); ?></h3>
					<div class="woocommerce-address-fields">
						<?php do_action( "woocommerce_before_edit_address_form_{$load_address}" ); ?>
						<div class="woocommerce-address-fields__field-wrapper">
							<?php
								foreach ( $address as $key => $field ) {
									if ( isset( $field['country_field'], $address[ $field['country_field'] ] ) ) {
										$field['country'] = wc_get_post_data_by_key( $field['country_field'], $address[ $field['country_field'] ]['value'] );
									}
									woocommerce_form_field( $key, $field, wc_get_post_data_by_key( $key, $field['value'] ) );
								}
							?>
						</div>
						<?php do_action( "woocommerce_after_edit_address_form_{$load_address}" ); ?>
						<p>
							<input type="submit" class="button" name="save_order_billing_address" value="<?php esc_attr_e( 'Save Order Billing Address', 'woocommerce' ); ?>" />
							<?php wp_nonce_field( 'woocommerce-edit_order_billing_address' ); ?>
							<input type="hidden" name="action" value="edit_order_billing_address" />
							<input type="hidden" name="pbd_order_id" value="<?php echo $order->ID ;?>" />
						</p>
					</div>
				</form>
			</div><!-- .medium-8-->
		</div><!-- .row-->
		
		<?php
		} else {
			echo 'No Order ID.';
		}
	}
	/**
	 * Plugin install action.
	 * Flush rewrite rules to make our custom endpoint available.
	 */
	public static function install() {
		flush_rewrite_rules();
	}
}
new Pbd_My_Account_Edit_Order_Billing_Address_Endpoint();
register_activation_hook( __FILE__, array( 'Pbd_My_Account_Edit_Order_Billing_Address_Endpoint', 'install' ) );

class Pbd_My_Account_Edit_Order_Shipping_Address_Endpoint {
	/**
	 * Custom endpoint name.
	 *
	 * @var string
	 */
	public static $endpoint = 'edit-order-shipping-address';
	/**
	 * Plugin actions.
	 */
	public function __construct() {
		// Actions used to insert a new endpoint in the WordPress.
		add_action( 'init', array( $this, 'add_endpoints' ) );
		add_filter( 'query_vars', array( $this, 'add_query_vars' ), 0 );
		// Change the My Accout page title.
		add_filter( 'the_title', array( $this, 'endpoint_title' ) );
		// Inserting your new tab/page into the My Account page.
		add_filter( 'woocommerce_account_menu_items', array( $this, 'new_menu_items' ) );
		add_action( 'woocommerce_account_' . self::$endpoint .  '_endpoint', array( $this, 'endpoint_content' ) );
	}
	/**
	 * Register new endpoint to use inside My Account page.
	 *
	 * @see https://developer.wordpress.org/reference/functions/add_rewrite_endpoint/
	 */
	public function add_endpoints() {
		add_rewrite_endpoint( self::$endpoint, EP_ROOT | EP_PAGES );
		add_action( 'template_redirect', array( __CLASS__, 'save_order_shipping_address' ) );
	}
	/**
	 * Add new query var.
	 *
	 * @param array $vars
	 * @return array
	 */
	public function add_query_vars( $vars ) {
		$vars[] = self::$endpoint;
		$vars[] = 'edit_order_shipping_address';
		$vars[] = 'pbd_order_id';
		return $vars;
	}
	/**
	 * Set endpoint title.
	 *
	 * @param string $title
	 * @return string
	 */
	public function endpoint_title( $title ) {
		global $wp_query;
		$is_endpoint = isset( $wp_query->query_vars[ self::$endpoint ] );
		if ( $is_endpoint && ! is_admin() && is_main_query() && in_the_loop() && is_account_page() ) {
			// New page title.
			$title = __( 'Edit Order Shipping Address', 'woocommerce' );
			remove_filter( 'the_title', array( $this, 'endpoint_title' ) );
		}
		return $title;
	}

	/**
	 * Save and and update a order shipping  address
	 */
	public static function save_order_shipping_address() {
		global $wp;
		
		if ( 'POST' !== strtoupper( $_SERVER['REQUEST_METHOD'] ) ) {
			return;
		}

		if ( empty( $_POST['action'] ) || 'edit_order_shipping_address' !== $_POST['action'] || empty( $_POST['_wpnonce'] ) || ! wp_verify_nonce( $_POST['_wpnonce'], 'woocommerce-edit_order_shipping_address' ) ) {
			return;
		}
		//var_dump($_POST);
		$order_id = $wp->query_vars['pbd_order_id'];
		//var_dump('run save_order_shipping_address '.$order_id); die();
		wc_nocache_headers();

		$user_id = get_current_user_id();

		if ( $user_id <= 0 ) {
			return;
		}

		$load_address = 'shipping';

		$address = WC()->countries->get_address_fields( esc_attr( $_POST[ $load_address . '_country' ] ), $load_address . '_' );

		foreach ( $address as $key => $field ) {

			if ( ! isset( $field['type'] ) ) {
				$field['type'] = 'text';
			}

			// Get Value.
			switch ( $field['type'] ) {
				case 'checkbox' :
					$_POST[ $key ] = (int) isset( $_POST[ $key ] );
					break;
				default :
					$_POST[ $key ] = isset( $_POST[ $key ] ) ? wc_clean( $_POST[ $key ] ) : '';
					break;
			}

			// Hook to allow modification of value.
			$_POST[ $key ] = apply_filters( 'woocommerce_process_myaccount_field_' . $key, $_POST[ $key ] );

			// Validation: Required fields.
			if ( ! empty( $field['required'] ) && empty( $_POST[ $key ] ) ) {
				wc_add_notice( sprintf( __( '%s is a required field.', 'woocommerce' ), $field['label'] ), 'error' );
			}

			if ( ! empty( $_POST[ $key ] ) ) {

				// Validation rules.
				if ( ! empty( $field['validate'] ) && is_array( $field['validate'] ) ) {
					foreach ( $field['validate'] as $rule ) {
						switch ( $rule ) {
							case 'postcode' :
								$_POST[ $key ] = strtoupper( str_replace( ' ', '', $_POST[ $key ] ) );
								if ( ! WC_Validation::is_postcode( $_POST[ $key ], $_POST[ $load_address . '_country' ] ) ) {
									wc_add_notice( __( 'Please enter a valid postcode / ZIP.', 'woocommerce' ), 'error' );
								} else {
									$_POST[ $key ] = wc_format_postcode( $_POST[ $key ], $_POST[ $load_address . '_country' ] );
								}
								break;
							case 'phone' :
								$_POST[ $key ] = wc_format_phone_number( $_POST[ $key ] );
								if ( ! WC_Validation::is_phone( $_POST[ $key ] ) ) {
									wc_add_notice( sprintf( __( '%s is not a valid phone number.', 'woocommerce' ), '<strong>' . $field['label'] . '</strong>' ), 'error' );
								}
								break;
							case 'email' :
								$_POST[ $key ] = strtolower( $_POST[ $key ] );
								if ( ! is_email( $_POST[ $key ] ) ) {
									wc_add_notice( sprintf( __( '%s is not a valid email address.', 'woocommerce' ), '<strong>' . $field['label'] . '</strong>' ), 'error' );
								}
								break;
						}
					}
				}
			}
		}

		do_action( 'woocommerce_after_save_address_validation', $user_id, $load_address, $address );

		if ( 0 === wc_notice_count( 'error' ) ) {
			$order = wc_get_order( $order_id );
			if($order) {
				//var_dump(get_post_meta($order_id));die();
				if(isset($_POST['shipping_first_name']) && strlen($_POST['shipping_first_name'])>1) { $order->set_shipping_first_name($_POST['shipping_first_name']); }
				if(isset($_POST['shipping_last_name']) && strlen($_POST['shipping_last_name'])>1) { $order->set_shipping_last_name($_POST['shipping_last_name']); }
				if(isset($_POST['shipping_company']) && strlen($_POST['shipping_company'])>1) { $order->set_shipping_company($_POST['shipping_company']); }
				if(isset($_POST['shipping_address_1']) && strlen($_POST['shipping_address_1'])>1) { $order->set_shipping_address_1($_POST['shipping_address_1']); }
				if(isset($_POST['shipping_address_2']) && strlen($_POST['shipping_address_2'])>1) { $order->set_shipping_address_2($_POST['shipping_address_2']); }
				if(isset($_POST['shipping_city']) && strlen($_POST['shipping_city'])>1) { $order->set_shipping_city($_POST['shipping_city']); }
				if(isset($_POST['shipping_state']) && strlen($_POST['shipping_state'])>1) { $order->set_shipping_state($_POST['shipping_state']); }
				if(isset($_POST['shipping_postcode']) && strlen($_POST['shipping_postcode'])>1) { $order->set_shipping_postcode($_POST['shipping_postcode']); }
				if(isset($_POST['shipping_country']) && strlen($_POST['shipping_country'])>1) { $order->set_shipping_country($_POST['shipping_country']); }
				if(isset($_POST['shipping_email']) && strlen($_POST['shipping_email'])>1) { $order->set_shipping_email($_POST['shipping_email']); }
				if(isset($_POST['shipping_phone']) && strlen($_POST['shipping_phone'])>1) { 
				//$order->set_address_prop('phone', 'shipping', $_POST['shipping_phone']); 
				update_post_meta($order_id, 'shipping_phone', $_POST['shipping_phone']);
				}
				$order->save();
			}
			wc_add_notice( __( 'Order Shipping Address changed successfully.', 'woocommerce' ) );

			do_action( 'woocommerce_customer_save_order_shipping_address', $user_id, $load_address );

			wp_safe_redirect( wc_get_endpoint_url( 'view-order', $order_id, wc_get_page_permalink( 'myaccount' ) ) );
			exit;
		}
	}




	/**
	 * Insert the new endpoint into the My Account menu.
	 *
	 * @param array $items
	 * @return array
	 */
	public function new_menu_items( $items ) {
		// Remove the logout menu item.
		$logout = $items['customer-logout'];
		unset( $items['customer-logout'] );
		// Insert your custom endpoint.
		$items[ self::$endpoint ] = __( 'Edit Order Shipping Address', 'woocommerce' );
		// Insert back the logout item.
		$items['customer-logout'] = $logout;
		return $items;
	}
	/**
	 * Endpoint HTML content.
	 */
	public function endpoint_content($var) {
		if($var) {
			$order = wc_get_order( $var );
			$order_id = $order->id;
			global $woocommerce, $current_user;
			$page_title = esc_html__( 'Shipping Address', 'woocommerce' );
			$current_user = wp_get_current_user();
			$load_address = 'shipping';
			$address = WC()->countries->get_address_fields( get_user_meta( get_current_user_id(), 'shipping_country', true ), 'shipping_' );
			$addressx = $order->get_address( 'shipping' );

			// Enqueue scripts
			wp_enqueue_script( 'wc-country-select' );
			wp_enqueue_script( 'wc-address-i18n' );

			// Prepare values
			foreach ( $address as $key => $field ) {
				$value = $addressx[str_replace('shipping_','',$key)];
				if ( ! $value ) {
					switch ( $key ) {
						case 'shipping_email' :
						case 'shipping_email' :
							$value = $current_user->user_email;
						break;
						case 'shipping_country' :
						case 'shipping_country' :
							$value = WC()->countries->get_base_country();
						break;
						case 'shipping_state' :
						case 'shipping_state' :
							$value = WC()->countries->get_base_state();
						break;
						case 'shipping_phone' :
							$value = get_post_meta($order_id, 'shipping_phone', true);
						break;
					}
				}
				//unset($address[ $key ]);
				$address[ $key ]['value'] = $value;
				//$address[ $key ]['value'] = apply_filters( 'woocommerce_my_account_edit_address_field_value', $field, $key, $load_address );
			}
			
			//$address = apply_filters( 'woocommerce_address_to_edit', $address, $load_address );
		 ?>
		<style>
		.site_header.with_featured_img,
		.site_header.without_featured_img {
		margin-bottom: 50px;
		}
		</style>

		<div class="row">
			<div class="large-9 large-centered columns">
				<form method="post">
					<h3><?php echo apply_filters( 'woocommerce_my_account_edit_address_title', $page_title, $load_address ); ?></h3>
					<div class="woocommerce-address-fields">
						<?php do_action( "woocommerce_before_edit_address_form_{$load_address}" ); ?>
						<div class="woocommerce-address-fields__field-wrapper">
							<?php
								foreach ( $address as $key => $field ) {
									if ( isset( $field['country_field'], $address[ $field['country_field'] ] ) ) {
										$field['country'] = wc_get_post_data_by_key( $field['country_field'], $address[ $field['country_field'] ]['value'] );
									}
									woocommerce_form_field( $key, $field, wc_get_post_data_by_key( $key, $field['value'] ) );
								}
							?>
						</div>
						<?php do_action( "woocommerce_after_edit_address_form_{$load_address}" ); ?>
						<p>
							<input type="submit" class="button" name="save_order_shipping_address" value="<?php esc_attr_e( 'Save Order Shipping Address', 'woocommerce' ); ?>" />
							<?php wp_nonce_field( 'woocommerce-edit_order_shipping_address' ); ?>
							<input type="hidden" name="action" value="edit_order_shipping_address" />
							<input type="hidden" name="pbd_order_id" value="<?php echo $order->ID ;?>" />
						</p>
					</div>
				</form>
			</div><!-- .medium-8-->
		</div><!-- .row-->
		

		<?php
		} else {
			echo 'No Order ID.';
		}
	}
	/**
	 * Plugin install action.
	 * Flush rewrite rules to make our custom endpoint available.
	 */
	public static function install() {
		flush_rewrite_rules();
	}
}
new Pbd_My_Account_Edit_Order_Shipping_Address_Endpoint();
register_activation_hook( __FILE__, array( 'Pbd_My_Account_Edit_Order_Shipping_Address_Endpoint', 'install' ) );
