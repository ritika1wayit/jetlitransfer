<?php
/**
 * Haiti
 *
 * @package WooCommerce/i18n
 * @version 2.0.0
 */

global $states;

defined( 'ABSPATH' ) || exit;
$user = new WP_User(get_current_user_id());
if(in_array('customer',$user->roles))
 {
  
 $states['HT'] = array(
	'PA' => __( 'Port au Prince', 'woocommerce' ),
	'LE' => __( 'Leoganne', 'woocommerce' ),

	
);

 } 
 
 else
 {
   $states['HT'] = array(
	'PA' => __( 'Port au Prince', 'woocommerce' ),
	'LE' => __( 'Leoganne', 'woocommerce' ),
	'CH' => __( 'Cap Haitien', 'woocommerce' ),
	'PT' => __( 'Cayes', 'woocommerce' ),
	'CB' => __( 'Cabaret', 'woocommerce' ),
	'GO' => __( 'Gonaives', 'woocommerce' ),
	'HE' => __( 'Hinche', 'woocommerce' ),
	'MI' => __( 'Mirebalais', 'woocommerce' ),
	'PG' => __( 'Petit Goaves', 'woocommerce' ),
	'SM' => __( 'St Marc', 'woocommerce' ),

	
);

 } 
