<?php
/**
 * Login Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.6
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$tdl_options = woodstock_global_var();
?>

<style>
.site-content {
	margin-top: 80px;
	margin-bottom: 80px;
}
</style>

<?php wc_print_notices(); ?>

<div class="row">
	<div class="medium-10 medium-centered large-6 large-centered columns">
		
		<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

		<div class="login-register-container">
				
			<div class="row">
				
			
				<div class="large-12 columns">
					<div class="account-forms-container">
						<ul class="account-tab-list">
							
							<li class="account-tab-item"><a class="account-tab-link current" href="#login"><?php esc_html_e( 'Login', 'woocommerce' ); ?></a></li>
							
							<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>
								<li class="account-tab-item last"><a class="account-tab-link" href="#register"><?php esc_html_e( 'Register', 'woocommerce' ); ?></a></li>
							<?php endif; ?>
						
						</ul>
						
						<div class="account-forms">
							<form id="login" method="post" class="login-form">
					
								<?php do_action( 'woocommerce_login_form_start' ); ?>
					
								<p class="form-row form-row-wide">
									<label for="username"><?php esc_html_e( 'Username or email address', 'woocommerce' ); ?> <span class="required">*</span></label>
									<input type="text" class="input-text" name="username" id="username"  placeholder="<?php esc_html_e( 'Username or email address', 'woocommerce' ); ?>" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
								</p>
								<p class="form-row form-row-wide">
									<label for="password"><?php esc_html_e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
									<input class="input-text" type="password" name="password" id="password" placeholder="<?php esc_html_e( 'Password', 'woocommerce' ); ?>" />
								</p>
					
								<?php do_action( 'woocommerce_login_form' ); ?>
					
								<p class="form-row">
									<?php wp_nonce_field( 'woocommerce-login' ); ?>

									<input type="submit" class="button" name="login" value="<?php esc_html_e( 'Login', 'woocommerce' ); ?>" /> 
									
									<label for="rememberme" class="inline remember-me check_label">
										<input name="rememberme" class="check_box" type="checkbox" id="rememberme" value="forever" /> 
										<?php esc_html_e( 'Remember me', 'woocommerce' ); ?>
									</label>
									
								</p>
								<p class="lost_password">
									<a class="lost-pass-link" href="<?php echo esc_url( wc_lostpassword_url() ); ?>">
										<?php esc_html_e( 'Lost your password?', 'woocommerce' ); ?>
									</a>
								</p>
					
								<?php do_action( 'woocommerce_login_form_end' ); ?>
					
							</form>
							
						<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>
								
							<form id="register" method="post" class="register">
					
								<?php do_action( 'woocommerce_register_form_start' ); ?>
					
								<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>
					
									<p class="form-row form-row-wide">
										<label for="reg_username"><?php esc_html_e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label>
										<input type="text" class="input-text" name="username" id="reg_username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
									</p>
					
								<?php endif; ?>
					
								<p class="form-row form-row-wide">
									<label for="reg_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>		
									<input type="email" class="input-text" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" placeholder="<?php esc_html_e( 'Email address', 'woocommerce' ); ?>"/>
								</p>
					
								<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>
	
								<p class="form-row form-row-wide">
									<label for="reg_password"><?php esc_html_e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
									<input type="password" class="input-text" name="password" id="reg_password" placeholder="<?php esc_html_e( 'Password', 'woocommerce' ); ?>" />
								</p>
					
								<?php endif; ?>

								<!-- Spam Trap -->
								<div style="left:-999em; position:absolute;"><label for="trap"><?php esc_html_e( 'Anti-spam', 'woocommerce' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" /></div>
					
								<?php do_action( 'woocommerce_register_form' ); ?>
								<?php do_action( 'register_form' ); ?>
					
								<p class="form-row">
									<?php wp_nonce_field( 'woocommerce-register' ); ?>
									<input type="submit" class="button" name="register" value="<?php esc_html_e( 'Register', 'woocommerce' ); ?>" />
								</p>
					
								<?php do_action( 'woocommerce_register_form_end' ); ?>
					
							</form>					
								
						<?php endif; ?>
                        	
						</div><!-- .account-forms-->
					</div><!-- .account-forms-container-->
				</div><!-- .medium-8-->
			</div><!-- .row-->
		</div><!-- .login-register-container-->
		
		<?php do_action( 'woocommerce_after_customer_login_form' ); ?>

	</div><!-- .large-6-->
</div><!-- .rows-->
