<?php
/**
 * Shipping Calculator
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.8
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( get_option( 'woocommerce_enable_shipping_calc' ) === 'no' || ! WC()->cart->needs_shipping() ) {
	return;
}

?>

<?php do_action( 'woocommerce_before_shipping_calculator' ); ?>
<style>
  section.shipping-calculator-form {
    display: block !important;
}
a.shipping-calculator-button {
    display: none !important;
}
.select2-container .select2-arrow {
   
    padding-right: 0px !important;
}
</style>
<form class="woocommerce-shipping-calculator" id="custom-update" action="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>" method="post">
<!--<p style="font-size: 12px;-->
<!--    color: #fb0808;-->
<!--    font-weight: bold;-->
<!--    padding: -4px;-->
<!--    line-height: 16px;margin-bottom: -0.75rem;padding-top:10px;" class="confirm-text">* Please Select Delivery City</p>-->
	<p class="shipping-calculator-button-wrapper"><a href="#" class="shipping-calculator-button"><?php esc_html_e( 'Confirm Delivery City', 'woocommerce' ); ?></a></p>

	<section class="shipping-calculator-form" style="display:none;">

		<p class="form-row form-row-wide">
			<select name="calc_shipping_country" id="calc_shipping_country" class="country_to_state" rel="calc_shipping_state">
				<option value=""><?php esc_html_e( 'Select a country&hellip;', 'woocommerce' ); ?></option>
				<?php
					foreach( WC()->countries->get_shipping_countries() as $key => $value )
						echo '<option value="' . esc_attr( $key ) . '"' . selected( WC()->customer->get_shipping_country(), esc_attr( $key ), false ) . '>' . esc_html( $value ) . '</option>';
				?>
			</select>
		
		</p>

		<p class="form-row form-row-wide">
			<?php
				$current_cc = WC()->customer->get_shipping_country();
				$current_r  = WC()->customer->get_shipping_state();
				$states     = WC()->countries->get_states( $current_cc );

				// Hidden Input
				if ( is_array( $states ) && empty( $states ) ) {

					?><input type="hidden" name="calc_shipping_state" id="calc_shipping_state" placeholder="<?php esc_html_e( 'State / county', 'woocommerce' ); ?>" /><?php

				// Dropdown Input
				} elseif ( is_array( $states ) ) {

					?><span>
						<select name="calc_shipping_state" class="checkship_state" id="calc_shipping_state" placeholder="<?php esc_attr_e( 'State / county', 'woocommerce' ); ?>">
							<option value=""><?php esc_html_e( 'Select Delivery City', 'woocommerce' ); ?></option>
							<?php
								foreach ( $states as $ckey => $cvalue )
									echo '<option value="' . esc_attr( $ckey ) . '" ' . selected( $current_r, $ckey, false ) . '>' . esc_html__( esc_html( $cvalue ), 'woocommerce' , 'woocommerce' ) .'</option>';
							?>
						</select>
						<div class="city_validate"></div>
							<input type="hidden" value="1" name="checkVals" class="checkvals"/>
					</span><?php

				// Standard Input
				} 
			?>
		</p>

		<?php if ( apply_filters( 'woocommerce_shipping_calculator_enable_city', true ) ) : ?>

			<p class="form-row form-row-wide">
				<input type="text" class="input-text" value="<?php echo esc_attr( WC()->customer->get_shipping_city() ); ?>" placeholder="<?php esc_html_e( 'City', 'woocommerce' ); ?>" name="calc_shipping_city" id="calc_shipping_city" />
			</p>

		<?php endif; ?>

		<?php if ( apply_filters( 'woocommerce_shipping_calculator_enable_postcode', true ) ) : ?>

			<p class="form-row form-row-wide">
				<input type="text" class="input-text" value="<?php echo esc_attr( WC()->customer->get_shipping_postcode() ); ?>" placeholder="<?php esc_html_e( 'Postcode / Zip', 'woocommerce' ); ?>" name="calc_shipping_postcode" id="calc_shipping_postcode" />
			</p>

		<?php endif; ?>

		<p><button type="submit" name="calc_shipping"  id="autoUpdateCity" value="1" class="button autoUpdateCity updateshippingprice"><?php esc_html_e( 'Update City', 'woocommerce' ); ?></button></p>
       
		<?php wp_nonce_field( 'woocommerce-cart' ); ?>
	</section>
</form>

<?php do_action( 'woocommerce_after_shipping_calculator' ); ?>
