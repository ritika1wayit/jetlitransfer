<?php
/**
 * Checkout Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

?>

<style>
.site_header.with_featured_img,
.site_header.without_featured_img {
	margin-bottom: 50px;
}
</style>
<?php $set_checkout =  WC()->cart->get_cart_total();// print_r($set_checkout);?>



<?php
do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', esc_html__( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}

// filter hook for include new pages inside the payment method
$get_checkout_url = apply_filters( 'woocommerce_get_checkout_url', WC()->cart->get_checkout_url() ); ?>

<div class="row">
    <div class="large-12 columns">

        <form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( $get_checkout_url ); ?>">

            <?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>

                <?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>
				<div class="row">

					<div class="large-7 columns">
						<div class="checkout_left_wrapper">

							<div class="col2-set" id="customer_details">

								<div class="col-1">

									<?php do_action( 'woocommerce_checkout_billing' ); ?>

								</div>

								<div class="col-2">

									<?php do_action( 'woocommerce_checkout_shipping' ); ?>

								</div>

							</div>

							<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

						</div><!--.checkout_left_wrapper-->
					</div><!--.large-7-->

					<div class="large-5 columns">
						<div class="checkout_right_wrapper bordered">
							<div class="order_review_wrapper">

								<h3 id="order_review_heading"><?php esc_html_e( 'Your order', 'woocommerce' ); ?></h3>

								<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

								<div id="order_review" class="woocommerce-checkout-review-order">
									<?php do_action( 'woocommerce_checkout_order_review' ); ?>
								</div>

								<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

							</div><!--.order_review_wrapper-->
						</div><!--.checkout_right_wrapper-->
					</div><!--.large-5-->
				</div><!--.row-->

            <?php endif; ?>

        </form>
   	<script>
		//document.getElementById('billing_phone').value = '+509';
		document.getElementById('shipping_phone').value = '+509';
                //document.getElementById('shipping_phone_27002').value = '+509';
	</script>
	<script>
		document.getElementById('billing_city').value = ' ';
		document.getElementById('shipping_city').value = 'Port au Prince ';
	</script>
<script>
		document.getElementById('billing_city').value = ' ';
		document.getElementById('shipping_city').value = 'Port au Prince ';
	</script>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script type='text/javascript'>
        $(function() {
            // Checking Address Length
            $('#shipping_phone').keyup(function() {
                var Address = $('#shipping_phone').val();
                var Addresslen = Address.length;
                if (Address.length >= 10) {
                    this.value = this.value.substring(0, 12);
                    return false;
                }
                $('#spanAddress').text(10 - Addresslen + ' Characters Left');

            });
        });
    </script>



    </div><!-- .columns -->
</div><!-- .row -->

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
