<?php
$tdl_options = woodstock_global_var();
$footer_logos = $tdl_options['tdl_footer_logos_off'];
$number_of_widgets = $tdl_options['tdl_footer_layout'];

		if ( $number_of_widgets == 4 ) {
			$grid_class = "large-3 medium-6 columns";
		}
		else if ( $number_of_widgets == 3 ) {
			$grid_class = "large-4 medium-6 columns";
		}
		else if ( $number_of_widgets == 2 ) {
			$grid_class = "large-6 medium-6 columns";
		}
		else if ( $number_of_widgets == 1 ) {
			$grid_class = "large-12 columns";
		}
?>

<footer id="site-footer" class="<?php echo esc_attr($tdl_options['tdl_footer_color_scheme']) ?>">

	<?php if( $number_of_widgets !== '0' ) { ?>
	<div class="f-columns shop_sidebar">

		<div class="row">

		    <?php if ( $number_of_widgets <= 4 ): ?>
		        <?php for ( $i = 1; $i <= $number_of_widgets; $i++ ) { ?>
		            <section class="<?php echo esc_attr($grid_class);?> column-widget">
		            <?php if ( is_active_sidebar( 'footer-sidebar-' . $i ) ) { ?><?php dynamic_sidebar( 'footer-sidebar-' . $i ); ?><?php } ?>
		            </section>
		        <?php } // end foreach ?>
		    <?php endif; ?>

		    <?php if ( $number_of_widgets == 5 ) : ?>

		    <section class="large-3 medium-4 columns column-widget">
				<?php if ( is_active_sidebar( 'footer-sidebar-1' ) ) { ?><?php dynamic_sidebar( 'footer-sidebar-1' ); ?><?php } ?>
		    </section>
		    <section class="large-3 medium-4 columns column-widget">
				<?php if ( is_active_sidebar( 'footer-sidebar-2' ) ) { ?><?php dynamic_sidebar( 'footer-sidebar-2' ); ?><?php } ?>
		    </section>
		    <section class="large-6 medium-4 columns column-widget">
				<?php if ( is_active_sidebar( 'footer-sidebar-3' ) ) { ?><?php dynamic_sidebar( 'footer-sidebar-3' ); ?><?php } ?>
		    </section>

		    <?php endif; ?>

		    <?php if ( $number_of_widgets == 6 ) : ?>

		    <section class="large-3 medium-4 columns column-widget">
				<?php if ( is_active_sidebar( 'footer-sidebar-1' ) ) { ?><?php dynamic_sidebar( 'footer-sidebar-1' ); ?><?php } ?>
		    </section>
		    <section class="large-6 medium-4 columns column-widget">
				<?php if ( is_active_sidebar( 'footer-sidebar-2' ) ) { ?><?php dynamic_sidebar( 'footer-sidebar-2' ); ?><?php } ?>
		    </section>
		    <section class="large-3 medium-4 columns column-widget">
				<?php if ( is_active_sidebar( 'footer-sidebar-3' ) ) { ?><?php dynamic_sidebar( 'footer-sidebar-3' ); ?><?php } ?>
		    </section>

		    <?php endif; ?>

		    <?php if ( $number_of_widgets == 7 ) : ?>

		    <section class="large-6 medium-4 columns column-widget">
				<?php if ( is_active_sidebar( 'footer-sidebar-1' ) ) { ?><?php dynamic_sidebar( 'footer-sidebar-1' ); ?><?php } ?>
		    </section>
		    <section class="large-3 medium-4 columns column-widget">
				<?php if ( is_active_sidebar( 'footer-sidebar-2' ) ) { ?><?php dynamic_sidebar( 'footer-sidebar-2' ); ?><?php } ?>
		    </section>
		    <section class="large-3 medium-4 columns column-widget">
				<?php if ( is_active_sidebar( 'footer-sidebar-3' ) ) { ?><?php dynamic_sidebar( 'footer-sidebar-3' ); ?><?php } ?>
		    </section>

		    <?php endif; ?>
		</div>

	</div>
	<?php } ?>

	<?php if ( has_nav_menu( 'footer-navigation' ) ) : ?>
		<div id="footer-navigation">
			<div class="row">
				<div class="large-12 columns">
					<nav id="site-navigation-footer" class="footer-navigation">
						<?php
							wp_nav_menu(array(
								'theme_location'  => 'footer-navigation',
								'fallback_cb'     => false,
								'container'       => false,
								'items_wrap'      => '<ul id="%1$s">%3$s</ul>',
							));
						?>
					</nav><!-- #site-navigation-footer -->
				</div>
			</div>
		</div>
	<?php endif; ?>


	<div class="f-copyright">
		<div class="row">
			<?php if ( $footer_logos == '1' ) : ?>
				<div class="large-6 columns copytxt"><p><?php echo wp_kses( $tdl_options['tdl_footer_text'], 'default' ); ?></p></div>
				<div class="large-6 columns cards">
				<img src="<?php echo esc_url($tdl_options['tdl_footer_logos']['url']); ?>" alt="" />
				</div>
				<?php else: ?>
				<div class="medium-12 columns copytxt"><p><?php echo wp_kses( $tdl_options['tdl_footer_text'], 'default' ); ?></p></div>
			<?php endif; ?>
		</div>
	</div>

</footer>

<?php if ( (isset($tdl_options['tdl_sticky_menu'])) && (trim($tdl_options['tdl_sticky_menu']) == "1" ) ) : ?>
	<!-- ******************************************************************** -->
    <!-- * Sticky Header **************************************************** -->
    <!-- ******************************************************************** -->
	<header id="header-st">
		<div class="row <?php echo esc_attr($tdl_options['tdl_sticky_color_scheme']); ?> <?php echo esc_attr($tdl_options['tdl_stickydrop_color_scheme']); ?>">
			<div class="large-12 columns">

				<div class="mobile-menu-button"><a><i class="mobile-menu-icon"></i><span class="mobile-menu-text"><?php esc_html_e( 'Menu', 'woodstock' )?></span></a></div>

				<!-- Main Navigation -->

				<div id="sticky-site-nav" class="l-nav h-nav">
					<div class="nav-container row">
		 				<nav id="st-nav" class="nav-holder">
							<?php if(!$tdl_options['tdl_uber_menu']): ?>
								<ul class="navigation menu tdl-navbar-nav mega_menu">
							<?php endif; ?>
								<?php

								if(!$tdl_options['tdl_uber_menu']) {
									echo woodstock_mega_menu();
								} else {
								}
								?>
							<?php if(!$tdl_options['tdl_uber_menu']): ?>
								</ul>
							<?php endif; ?>
						</nav>
					</div>
				</div>	<!-- End Main Navigation -->

			<?php if (class_exists('WooCommerce')) : ?>
				<?php if ( (isset($tdl_options['tdl_catalog_mode'])) && ($tdl_options['tdl_catalog_mode'] == 0) ) : ?>
					<!-- Shop Section -->
					<div class="shop-bag">
						<a>
							<div class="l-header-shop">
								<span class="shopbag_items_number"><?php echo WC()->cart->cart_contents_count; ?></span>
								<i class="icon-shop"></i>
								<div class="overview">
									<span class="bag-items-number"><?php echo sprintf (_n( '%d item', '%d items', WC()->cart->cart_contents_count, 'woodstock' ), WC()->cart->cart_contents_count ); ?></span>
									<?php echo WC()->cart->get_cart_total(); ?>
								</div>
							</div>
						</a>
					</div>
				<?php endif; ?>
			<?php endif; ?>


			</div>

		</div>
    </header>
<?php endif; ?>

</div><!-- /boxed-layout -->
</div><!-- /page-wrap -->

</div><!-- /off-content -->
</div><!-- /off-drop -->

	<nav class="off-menu st-mobnav slide-from-left <?php echo esc_attr($tdl_options['tdl_sidebarnav_color_scheme']); ?>">
		<div class="nano">
			<div class="nano-content">
				<div id="mobiles-menu-offcanvas" class="offcanvas-left-content">
					<!-- Close Icon -->
					<a href="#" class="close-icon"></a>
					<div class="clearfix"></div>

					<?php if ( (isset($tdl_options['tdl_header_search_bar'])) && ($tdl_options['tdl_header_search_bar'] == "1") ) : ?>
							<!-- Search Section -->
					        <div class="l-search">

							<?php
							$ajax_url = admin_url( 'admin-ajax.php' );
							$header_search_type = $tdl_options['tdl_header_ajax_search'];
							$header_search_pt = $tdl_options['tdl_header_search_pt'];
							?>

					        <?php if ($header_search_type == 1) { ?>
								<div class="ajax-search-wrap search-wrap ajaxsrch" data-ajaxurl="<?php echo esc_url($ajax_url); ?>">
									<div class="ajax-loading <?php echo esc_attr($tdl_options['tdl_header_ajax_loader']); ?>"><div class="spinner"></div></div>
										<form method="get" class="ajax-search-form" action="<?php echo home_url() ?>/">
										<?php if ( $header_search_pt != "any" ) { ?>
											<input type="hidden" name="post_type" value="<?php echo esc_attr($header_search_pt); ?>" />
										<?php } ?>
											<input class="ajax-search-input" type="text" placeholder="<?php echo esc_html__( "Search", "woodstock" ) ?>" name="s" autocomplete="off" />
											<button class="ajax-search-submit" type="submit"></button>
										</form>
									<div class="ajax-search-results"></div>
								</div>
					        <?php } else { ?>
					        	<div class="ajax-search-wrap search-wrap" data-ajaxurl="">
					        		<form method="get" class="ajax-search-form" action="<?php echo home_url() ?>/">
										<?php if ( $header_search_pt != "any" ) { ?>
											<input type="hidden" name="post_type" value="<?php echo esc_attr($header_search_pt); ?>" />
										<?php } ?>
											<input class="ajax-search-input" type="text" placeholder="<?php echo esc_html__( "Search", "woodstock" ) ?>" name="s" autocomplete="off" />
											<button class="ajax-search-submit" type="submit"></button>
									</form>
					        	</div>
					        <?php } ?>

					        </div>
					        <?php endif; ?>

							<?php if ( (isset($tdl_options['tdl_header_customer_bar'])) && ($tdl_options['tdl_header_customer_bar'] == "1") ) : ?>
								<!-- Contact Section -->
								<div class="contact-info">
									<div class="inside-content">
										<?php if ( (isset($tdl_options['tdl_header_contactbox_icon'])) && ($tdl_options['tdl_header_contactbox_icon'] != "none") ) : ?>
											<span class="contact-info-icon"></span>
										<?php endif; ?>
					 					<span class="contact-info-title">
											<?php if ( isset($tdl_options['tdl_header_customer_bar_subtitle']) ) : ?>
												<span class="contact-info-subtitle"><?php echo esc_attr($tdl_options['tdl_header_customer_bar_subtitle']); ?></span>
											<?php endif; ?>
											<?php echo esc_attr($tdl_options['tdl_header_customer_bar_title']); ?>
					 					</span>

					 					<?php if ( (isset($tdl_options['tdl_header_customer_bar_text'])) && (trim($tdl_options['tdl_header_customer_bar_text']) != "" ) ) : ?>
										<span class="contact-info-arrow"></span>

										<div class="inside-area">
											<div class="inside-area-content">
											<?php echo do_shortcode($tdl_options['tdl_header_customer_bar_text']); ?>
											<div class="after-clear"></div>
											</div>
										</div>
										<?php endif; ?>
									</div>
								</div>
							<?php endif; ?>

							<nav id="mobile-main-navigation" class="mobile-navigation">
								<?php
									wp_nav_menu(array(
										'theme_location'  => 'main_navigation',
										'fallback_cb'     => false,
										'container'       => false,
										'menu_id' => 'mob-main-menu',
										'items_wrap'      => '<ul id="%1$s">%3$s</ul>',
									));
								?>
		                    </nav>

							<?php
								$theme_locations  = get_nav_menu_locations();
								if (isset($theme_locations['top-bar-navigation'])) {
									$menu_obj = get_term($theme_locations['top-bar-navigation'], 'nav_menu');
								}

								if ( (isset($menu_obj->count) && ($menu_obj->count > 0)) || (is_user_logged_in()) ) {
								?>

		                        <nav id="mobile-top-bar-navigation" class="mobile-navigation">
		                            <?php
		                                wp_nav_menu(array(
		                                    'theme_location'  => 'top-bar-navigation',
		                                    'fallback_cb'     => false,
		                                    'container'       => false,
		                                    'items_wrap'      => '<ul id="%1$s">%3$s</ul>',
		                                ));
		                            ?>
		                        </nav>

					            <nav id="mobile-right-top-bar-navigation" class="mobile-navigation">
					                <ul id="mob-my-account">
					                <?php
					                if ( is_user_logged_in() ) { ?>
					                    <li class="menu-item-has-children"><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" class="acc-link"><i class="acc-icon"></i><?php esc_html_e('My Account', 'woocommerce'); ?></a>
					                    <ul class="sub-menu">
					                         <?php
					                            wp_nav_menu(array(
					                                'theme_location'  => 'myaccount-navigation',
					                                'fallback_cb'     => false,
					                                'container'       => false,
					                                'items_wrap'      => '<li><ul id="%1$s">%3$s</ul></li>',
					                            ));
					                        ?>
					                    </ul>
					                    </li>

					                <?php } else { ?>
					                	<li class="login-link"><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" class="acc-link"><i class="login-icon"></i><?php esc_html_e('Login / Register', 'woodstock'); ?></a></li>
					                <?php } ?>

					 				<?php echo woodstock_wishlist_topbar(); ?>
					                </ul>
					            </nav><!-- .myacc-navigation -->

		            		<?php } ?>


				<?php echo woodstock_mob_language_and_currency(); ?>

		        <?php if ( (isset($tdl_options['tdl_topbar_social_icons'])) && (trim($tdl_options['tdl_topbar_social_icons']) == "1" ) ) : ?>
		            <div class="sidebar-social-icons-wrapper">
		                <ul class="social-icons">
		                    <?php if ( (isset($tdl_options['twitter_link'])) && (trim($tdl_options['twitter_link']) != "" ) ) { ?><li class="twitter"><a target="_blank" title="Twitter" href="<?php echo esc_url($tdl_options['twitter_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['facebook_link'])) && (trim($tdl_options['facebook_link']) != "" ) ) { ?><li class="facebook"><a target="_blank" title="Facebook" href="<?php echo esc_url($tdl_options['facebook_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['googleplus_link'])) && (trim($tdl_options['googleplus_link']) != "" ) ) { ?><li class="googleplus"><a target="_blank" title="Google Plus" href="<?php esc_url($tdl_options['googleplus_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['pinterest_link'])) && (trim($tdl_options['pinterest_link']) != "" ) ) { ?><li class="pinterest"><a target="_blank" title="Pinterest" href="<?php echo esc_url($tdl_options['pinterest_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['vimeo_link'])) && (trim($tdl_options['vimeo_link']) != "" ) ) { ?><li class="vimeo"><a target="_blank" title="Vimeo" href="<?php echo esc_url($tdl_options['vimeo_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['youtube_link'])) && (trim($tdl_options['youtube_link']) != "" ) ) { ?><li class="youtube"><a target="_blank" title="YouTube" href="<?php echo esc_url($tdl_options['youtube_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['flickr_link'])) && (trim($tdl_options['flickr_link']) != "" ) ) { ?><li class="flickr"><a target="_blank" title="Flickr" href="<?php echo esc_url($tdl_options['flickr_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['skype_link'])) && (trim($tdl_options['skype_link']) != "" ) ) { ?><li class="skype"><a target="_blank" title="Skype" href="<?php echo esc_url($tdl_options['skype_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['behance_link'])) && (trim($tdl_options['behance_link']) != "" ) ) { ?><li class="behance"><a target="_blank" title="Behance" href="<?php echo esc_url($tdl_options['behance_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['dribbble_link'])) && (trim($tdl_options['dribbble_link']) != "" ) ) { ?><li class="dribbble"><a target="_blank" title="Dribbble" href="<?php echo esc_url($tdl_options['dribbble_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['tumblr_link'])) && (trim($tdl_options['tumblr_link']) != "" ) ) { ?><li class="tumblr"><a target="_blank" title="Tumblr" href="<?php echo esc_url($tdl_options['tumblr_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['linkedin_link'])) && (trim($tdl_options['linkedin_link']) != "" ) ) { ?><li class="linkedin"><a target="_blank" title="Linkedin" href="<?php echo esc_url($tdl_options['linkedin_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['github_link'])) && (trim($tdl_options['github_link']) != "" ) ) { ?><li class="github"><a target="_blank" title="Github" href="<?php echo esc_url($tdl_options['github_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['vine_link'])) && (trim($tdl_options['vine_link']) != "" ) ) { ?><li class="vine"><a target="_blank" title="Vine" href="<?php echo esc_url($tdl_options['vine_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['instagram_link'])) && (trim($tdl_options['instagram_link']) != "" ) ) { ?><li class="instagram"><a target="_blank" title="Instagram" href="<?php echo esc_url($tdl_options['instagram_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['dropbox_link'])) && (trim($tdl_options['dropbox_link']) != "" ) ) { ?><li class="dropbox"><a target="_blank" title="Dropbox" href="<?php echo esc_url($tdl_options['dropbox_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['rss_link'])) && (trim($tdl_options['rss_link']) != "" ) ) { ?><li class="rss"><a target="_blank" title="RSS" href="<?php echo esc_url($tdl_options['rss_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['stumbleupon_link'])) && (trim($tdl_options['stumbleupon_link']) != "" ) ) { ?><li class="stumbleupon"><a target="_blank" title="Stumbleupon" href="<?php echo esc_url($tdl_options['stumbleupon_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['paypal_link'])) && (trim($tdl_options['paypal_link']) != "" ) ) { ?><li class="paypal"><a target="_blank" title="Paypal" href="<?php echo esc_url($tdl_options['paypal_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['foursquare_link'])) && (trim($tdl_options['foursquare_link']) != "" ) ) { ?><li class="foursquare"><a target="_blank" title="Foursquare" href="<?php echo esc_url($tdl_options['foursquare_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['soundcloud_link'])) && (trim($tdl_options['soundcloud_link']) != "" ) ) { ?><li class="soundcloud"><a target="_blank" title="Soundcloud" href="<?php echo esc_url($tdl_options['soundcloud_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['spotify_link'])) && (trim($tdl_options['spotify_link']) != "" ) ) { ?><li class="spotify"><a target="_blank" title="Spotify" href="<?php echo esc_url($tdl_options['spotify_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['vk_link'])) && (trim($tdl_options['vk_link']) != "" ) ) { ?><li class="vk"><a target="_blank" title="VKontakte" href="<?php echo esc_url($tdl_options['vk_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['android_link'])) && (trim($tdl_options['android_link']) != "" ) ) { ?><li class="android"><a target="_blank" title="Android" href="<?php echo esc_url($tdl_options['android_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['apple_link'])) && (trim($tdl_options['apple_link']) != "" ) ) { ?><li class="apple"><a target="_blank" title="Apple" href="<?php echo esc_url($tdl_options['apple_link']); ?>"></a></li><?php } ?>
		                    <?php if ( (isset($tdl_options['windows_link'])) && (trim($tdl_options['windows_link']) != "" ) ) { ?><li class="windows"><a target="_blank" title="Windows" href="<?php echo esc_url($tdl_options['windows_link']); ?>"></a></li><?php } ?>
		                </ul>
		            </div>
		        <?php endif; ?>

				</div>

				<!-- Shop Sidebar Offcanvas -->
                    <div id="filters-offcanvas" class="offcanvas-left-content wpb_widgetised_column">
	 					<!-- Close Icon -->
						<a href="#" class="close-icon"></a>
						<div class="clearfix"></div>

						<?php if (class_exists('WooCommerce')) : ?>
							<?php if (is_product()) : ?>
								<?php if ( is_active_sidebar( 'widgets-product-page-listing' ) ) : ?>
									<?php dynamic_sidebar( 'widgets-product-page-listing' ); ?>
								<?php endif; ?>
							<?php elseif (is_shop()) : ?>
								<?php if ( is_active_sidebar( 'widgets-product-listing' ) ) : ?>
									<?php dynamic_sidebar( 'widgets-product-listing' ); ?>
								<?php endif; ?>
							<?php else: ?>
								<?php if ( is_active_sidebar( 'sidebar' ) ) : ?>
									<?php dynamic_sidebar( 'sidebar' ); ?>
								<?php endif; ?>
							<?php endif; ?>
						<?php else: ?>
								<?php if ( is_active_sidebar( 'sidebar' ) ) : ?>
									<?php dynamic_sidebar( 'sidebar' ); ?>
								<?php endif; ?>
						<?php endif; ?>

                    </div>

			</div>
		</div>
	</nav>

	<nav class="off-menu slide-from-right <?php echo esc_attr($tdl_options['tdl_sidebarcart_color_scheme']); ?>">
		<div class="nano">
			<div class="nano-content">
					<div id="minicart-offcanvas" class="offcanvas-right-content" data-empty-bag-txt="<?php esc_html_e( 'Your cart is currently empty.', 'woocommerce' ); ?>" data-singular-item-txt="<?php esc_html_e( 'item', 'woodstock' ); ?>" data-multiple-item-txt="<?php esc_html_e( 'items', 'woodstock' ); ?>">
					<div class="loading-overlay"><div class="spinner <?php echo esc_attr($tdl_options['tdl_header_ajax_loader']); ?>"></div></div>
					<?php if ( class_exists( 'WC_Widget_Cart' ) ) { the_widget( 'TDL_WC_Widget_Cart' ); } ?></div>
			</div>
		</div>
	</nav>

</div><!-- /off-container -->

		<!-- ******************************************************************** -->
		<!-- * Custom Footer JavaScript Code ************************************ -->
		<!-- ******************************************************************** -->

		<?php if ( (isset($tdl_options['tdl_custom_js_footer'])) && ($tdl_options['tdl_custom_js_footer'] != "") ) : ?>
			<?php echo do_shortcode($tdl_options['tdl_custom_js_footer']); ?>
		<?php endif; ?>


<?php
$user = new WP_User(get_current_user_id());

if($user->roles[0]=='agent'){
    echo '<style>
    li.payment_method_wc_cash_on_delivery_agents{
        display:block;
    }


    </style>';
}

     $user=wp_get_current_user();
    // print_r($user);
     //echo '<pre>'; print_r($user);
    if($user->roles[0] != 'agent' && $user->roles[0] != 'customer'){
        // if(in_array('agent',$user->roles)){
      ?>
      <style>
          p.buttons a {
    position: static !important;
}

      </style>
      <script>

          jQuery(document).ready(function(){
             jQuery('form.checkout.woocommerce-checkout').hide();
             jQuery('.checkout_login').hide();
             jQuery('.checkout_coupon_box').hide();

              jQuery('tr.shipping, tr.shipping123').remove();

          });

      </script>
    <?php }

?>

<script type='text/javascript'>
/* <![CDATA[ */

	jQuery(document).ready(function() {
          jQuery('input:radio').removeAttr('checked');
		jQuery("#prod_categories").change(function(){
			jQuery('#products_list').val('');
			var val=jQuery(this).val();

			jQuery.ajax({
		    url: "<?php echo site_url()?>/cat.php",
		     beforeSend: function(){
		        jQuery(".My-loader").css("display", "block");
		    },
		    complete: function(){
		        jQuery(".My-loader").css("display", "none");
		    },
		    type: "POST",
		    data : {val:val},
		    cache: false,
		    success: function(data){

		    		jQuery('#products_list').html(data);

    			}
    		});
		});

	});

/* ]]> */
</script>
 <?php if(is_page(34)){ ?>
 <script type='text/javascript'>
/* <![CDATA[ */

	jQuery(document).ready(function() {

		jQuery("#calc_shipping_state").change(function(){
		  var stateVal = jQuery(this).val();
		  //alert(stateVal);
		  if(stateVal)
		  {
		      jQuery(".My-loader").css("display", "block");
		  }

	     else
	     {
	         jQuery(".My-loader").css("display", "none");
	     }
		 //jQuery(".My-loader").css("display", "none");


    		});
		});



/* ]]> */
</script>
 <?php }?>
 <script>

     jQuery(document).ready(function() {

		jQuery("#dest_city").change(function(){

			var packg=jQuery(this).val();

			if(packg != 'PA')
			{
			    jQuery('#prod_categories').html('<option value="141" class="category">packages</option>');

			    var cat_id = '141';
			    	jQuery.ajax({
		    url: "<?php echo site_url()?>/cat.php",
		   beforeSend: function(){
		        jQuery(".My-loader").css("display", "block");
		    },
		    complete: function(){
		        jQuery(".My-loader").css("display", "none");
		    },

		    type: "POST",
		    data : {val:cat_id},
		    cache: false,
		    success: function(data){

		    	var test =jQuery('#products_list').html(data);
		    	//alert(test);

    			}
    		});
			}
			else{
			     jQuery('#prod_categories').html('<option value="0">--Select Categories--</option>');
			     jQuery('#prod_categories').append('<option value="137" class="category">Appliances Electronics</option>');
			     jQuery('#prod_categories').append('<option value="136" class="category">Food & Beverages</option>');
			     jQuery('#prod_categories').append('<option value="141" class="category">packages</option>');
			}
		});

	});
 </script>


 <script>
     jQuery(document).on('click','#place_order',function(){
          var checkstate = jQuery('#billing_stat').val();
          if(checkstate=='')
          {
               jQuery('#billing_stat').css('border','1.8px solid rgb(169, 68, 66)') ;
          }

       jQuery('#billing_phone_field.validate-phone input').css('border','1.8px solid rgb(169, 68, 66)') ;
     });



 </script>




         <script>
         jQuery(document).ready(function(){
            jQuery( document ).on( 'focus', ':input', function(){
                jQuery( this ).attr( 'autocomplete', 'off' );
            });
        });

         </script>

    <script>
    var dest_city="<?php echo $_SESSION['dest_city'];?>";

      jQuery(document).on('click','.checkout-button',function(){
          var calShipVal = jQuery('#calc_shipping_state').val();
          //alert(calShipVal);
          if(calShipVal =='')
          {
             // alert('empty');
              jQuery('.city_validate').html('<p>Please Select Delivery City</p>');
               jQuery('#s2id_calc_shipping_state').css('border','1.8px solid #960d0d') ;
                $('.checkout-button').css('pointer-events', 'inherit');


               return false;
          }
          var userRole = "<?php echo $user->roles[0]; ?>";
          if(userRole!="customer"){
              if(dest_city!=calShipVal)
              {
                  // alert('empty');
                    jQuery('.city_validate').html('<p>Please Select Same Delivery City</p>');
                   jQuery('#s2id_calc_shipping_state').css('border','1.8px solid #960d0d') ;
                    $('.checkout-button').css('pointer-events', 'inherit');


                   return false;
              }
              else
              {
                //  alert('true');
                  $('.checkout-button').css('pointer-events', 'none');
                 // return true;
              }
          }else{
              $('.checkout-button').css('pointer-events', 'none');
          }
     });


    </script>

    <script>


      jQuery(document).on('click','input#sbmt_btn',function(){

          var dest_city = jQuery('select#dest_city').val();
          //alert(calShipVal);
          if(dest_city =='')
          {
             // alert('empty');
             alert('Select Destination City');


               return false;
          }

     });
    </script>

    <script>
            jQuery(document).on('change','.checkship_state',function(){
              jQuery('.checkvals').val(2);
              if(jQuery('.checkvals').val() == 2)
              {
                setTimeout(function(){
                  jQuery(".autoUpdateCity").attr('type','submit');
                  jQuery('.cart-form').submit();
               }, 3000);
                //window.location.href = "https://www.jetlitransfer.com/dev/cart/";
              }
            });

            jQuery(document).on('click','.guest-custom',function(){
              jQuery('form#register').hide();
               jQuery('form.checkout.woocommerce-checkout').show();
               jQuery('.page-id-35 form.checkout.woocommerce-checkout').show();
               console.log();
               if(jQuery("#userCheckLogin").val()==false){
                   jQuery('tr.shipping').remove();
               }

            });

             jQuery(document).on('click','.guest-register',function(){

             jQuery('form#register').show();

            });
    </script>




<?php

 if(in_array('customer',$user->roles))
 {
     echo '<style> a.button.proceed-checkout, a.button.continue,p#shipping_all_city_field, a.button.view_cart.wc-forward, .page-id-35 form.checkout.woocommerce-checkout, .checkout_login
     , .woocommerce-info{display:block !important;}</style>';

      echo '<style>.page-id-35 tr.shipping, .button.checkout.wc-forward, a.continue-shopping.button.wc-forward.close-icon{display:none !important;}</style>';


 }

 else
 {
   // echo '<style>option[value=PT] {display: block;}</style>';
   // echo '<style>.product_infos a.button,.product_infos a.button.continue {display: none;}</style>';
 }

 ?>

<?php
 if(in_array('agent',$user->roles))
 {
     echo '<style> #minicart-offcanvas .widget_shopping_cart .buttons a.checkout{display:none;}</style>';
    echo '<style>.button.view_cart.wc-forward {display:block !important}</style>';

 }
 ?>
<script>
    jQuery(document).on('click','li#menu-item-5016',function() {

        window.location.href = "https://www.jetlitransfer.com/dev/"
   //location.reload();
});

</script>


		<?php wp_footer(); ?>
		<script src="<?php  bloginfo('template_url')?>/js/checkout.js"></script>
	</body>
</html>
