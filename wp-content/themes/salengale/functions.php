<?php

/**
 * Woodstock functions and definitions.
 *
 * Sets up the theme and provides some helper functions, which are used
 * in the theme. Others are attached to action and filter hooks in WordPress
 * to change core functionality.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
 *
 * @package WordPress
 * @subpackage Woodstock
 * @since 1.0
 */


define('WOODSTOCK_OPTIONS_NAME', 'woodstock_options_theme_customizer');
define('WOODSTOCK_THEME_PATH', get_template_directory());
define('WOODSTOCK_INCLUDES', get_template_directory() . '/includes');
define('WOODSTOCK_FUNCTIONS', get_template_directory() . '/functions');
define('WOODSTOCK_FRAMEWORK', get_template_directory() . '/framework');
define('WOODSTOCK_THEME_URI', get_template_directory_uri());
define('WOODSTOCK_THEME_ENABLED', true);


include_once( WOODSTOCK_INCLUDES . '/custom_styles.php'); // Custom Styles
include_once( WOODSTOCK_FUNCTIONS . '/theme_options.php' ); // Theme Options
include_once( WOODSTOCK_FUNCTIONS . '/woo_options.php' ); // Woocommerce Options


/******************************************************************************/
/****** Plugin recommendations ************************************************/
/******************************************************************************/
require_once( get_template_directory() . '/includes/tgm/class-tgm-plugin-activation.php' );
require_once( get_template_directory() . '/includes/tgm/plugins.php' );

/******************************************************************************/
/****** Add Font Awesome to Redux *********************************************/
/******************************************************************************/

function woodstock_newIconFont() {

    wp_register_style(
        'redux-font-awesome',
        get_template_directory_uri() . '/fonts/fontawesome/font-awesome.min.css',
        array(),
        time(),
        'all'
    );
    wp_enqueue_style( 'redux-font-awesome' );
}
add_action( 'redux/page/tdl_options/enqueue', 'woodstock_newIconFont' );

function woodstock_global_var(){
   global $tdl_options;
   return $tdl_options;
}

/*-----------------------------------------------------------------------------------*/
/* Revolution Slider set as Theme
/*-----------------------------------------------------------------------------------*/

add_action('acf/init', 'woodstock_cp_setup_acf_updates');
function woodstock_cp_setup_acf_updates() {
	acf_update_setting('show_updates', false);
}

add_action('vc_before_init', 'woodstock_vc_set_as_theme');
function woodstock_vc_set_as_theme() {
	if (function_exists( 'vc_manager' )){
		vc_manager()->disableUpdater(true);
		vc_manager()->setIsAsTheme(true);
	}

	vc_set_as_theme();
}

add_action( 'init', 'woodstock_set_revslider_as_theme' );
function woodstock_set_revslider_as_theme() {
	if ( ! function_exists( 'set_revslider_as_theme' )){ return; }
	set_revslider_as_theme();
}


/* ---------------------------------------------------------------- */
/* ACF theme fields
/* ---------------------------------------------------------------- */
require_once WOODSTOCK_THEME_PATH . '/autoimport/custom_metaboxes.php';

/* ---------------------------------------------------------------- */
/* Add ACF fallback
/* ---------------------------------------------------------------- */
if (! is_admin() && ! function_exists('get_field')) {
	function get_field($name) {
		return false;
	}
}

/* ---------------------------------------------------------------- */
/* Cache Google Webfonts
/* ---------------------------------------------------------------- */
add_action('wp_ajax_tdl_cache_google_webfonts', 'woodstock_cache_google_webfonts');
function woodstock_cache_google_webfonts() {
	$google_webfonts = isset($_POST['google_webfonts']) ? $_POST['google_webfonts'] : '';

	if(!empty($google_webfonts)) {
		set_transient('tdl_google_webfonts', $google_webfonts, DAY_IN_SECONDS);
	}

	die();
}

/* ---------------------------------------------------------------- */
/* Register Navigation
/* ---------------------------------------------------------------- */

register_nav_menu('main_navigation', 'Main Navigation');
register_nav_menu('top-bar-navigation', 'Top Bar Navigation');
register_nav_menu('myaccount-navigation', 'My Account Navigation');
register_nav_menu('footer-navigation', 'Footer Navigation');

add_filter('wp_nav_menu_args', 'woodstock_main_menu_args');

function woodstock_main_menu_args($args) {
	global $post;

	$c_pageID = '';

	if((get_option('show_on_front') && get_option('page_for_posts') && is_home()) ||
		(get_option('page_for_posts') && is_archive() && !is_post_type_archive())) {
		$c_pageID = get_option('page_for_posts');
	} else {
		if(isset($post)) {
			$c_pageID = $post->ID;
		}

		if(class_exists('Woocommerce')) {
			if(is_shop() || is_tax('product_cat') || is_tax('product_tag')) {
				$c_pageID = get_option('woocommerce_shop_page_id');
			}
		}
	}

	return $args;
}

/* ---------------------------------------------------------------- */
/* Initialize the mega menu framework
/* ---------------------------------------------------------------- */


	// Initialize the mega menu framework
	include_once( WOODSTOCK_FUNCTIONS . '/wstock-mega-menu-framework.php' ); // Woocommerce Options

	function woodstock_create_menu() {
		global $main_menu;

		@$main_menu = wp_nav_menu(array(
				'theme_location'	=> 'main_navigation',
				'depth'				=> 5,
				'container' 		=> false,
				'menu_id' 			=> 'nav',
				'items_wrap' 		=> '%3$s',
				'menu_class'        => 'nav tdl-navbar-nav 222',
				'fallback_cb'       => 'tldCoreFrontendWalker::fallback',
				'walker'            => new tdlCoreFrontendWalker(),
				'echo' 				=> false
		));
	}


add_action( 'template_redirect', 'woodstock_create_menu' );

function woodstock_mega_menu() {
	global $main_menu;
	echo $main_menu;
}


/*-----------------------------------------------------------------------------------*/
/*	Registers and loads styles
/*-----------------------------------------------------------------------------------*/


if ( ! function_exists('woodstock_theme_styles') ) :

	function woodstock_theme_styles () {
		if (!is_admin()) {
			global $tdl_options, $wp_styles;

			$theme_info = wp_get_theme();

			// Edit CSS within their files
			wp_register_style( 'stylesheet', get_stylesheet_uri(), array(), '1.0', 'all' );
			wp_register_style( 'wstock-app', get_template_directory_uri() .  "/css/app.css", array(), '1.0', null);

			wp_enqueue_style('tooltipster', get_template_directory_uri() . '/css/tooltipster.css', array(), '3.3.0', 'all' );
			wp_enqueue_style('fresco', get_template_directory_uri() . '/css/fresco/fresco.css', array(), '1.3.0', 'all' );
			wp_enqueue_style('easyzoom', get_template_directory_uri() . '/css/easyzoom.css', array(), '1.0', 'all' );
			wp_enqueue_style('swiper', get_template_directory_uri() . '/css/idangerous.swiper.css', array(), '2.5.1', 'all' );
			wp_enqueue_style('nanoscroller', get_template_directory_uri() . '/css/nanoscroller.css', array(), '0.7.6', 'all' );
			wp_enqueue_style('select2', get_template_directory_uri() . '/css/select2.css', array(), '3.5.1', 'all' );


			wp_enqueue_style('wstock-app');
			wp_enqueue_style('stylesheet' );

		wp_enqueue_style( 'wstock-IE', get_template_directory_uri() . '/css/ie.css', array(), $theme_info->get( 'Version' ) );
		$wp_styles->add_data( 'wstock-IE', 'conditional', 'IE' );

		}
	}
	add_action('wp_enqueue_scripts', 'woodstock_theme_styles', 99 );
endif;

/*-----------------------------------------------------------------------------------*/
/*	Registers and loads front-end javascript
/*-----------------------------------------------------------------------------------*/

if ( ! function_exists('woodstock_register_js') ) :

	function woodstock_register_js() {
		if (!is_admin()) {

			// Get theme version info
			global $woodstock_theme_info;

// 			wp_enqueue_script('google-maps', 'https://maps.googleapis.com/maps/api/js?sensor=false', array(), '1.0', FALSE);
		wp_enqueue_script('wstock-plugins', get_template_directory_uri() . '/js/wstock-plugins.js', array('jquery'), '1.1', TRUE);
		wp_enqueue_script('wstock-custom-scripts', get_template_directory_uri() . '/js/wstock-custom.scripts.js', array('jquery'), '1.0', TRUE);

			// Enqueue javascript used on every page
			// wp_enqueue_script('jquery');

			$woodstock_options = apply_filters( 'woodstock_localize_script_data', array(
				'ajaxurl'				=> admin_url( 'admin-ajax.php' ),
				'live_search_empty_msg'	=> esc_html__( 'Unable to find any products that match the currenty query', 'woodstock' )
			) );

			wp_localize_script( 'scripts', 'woodstock_options', $woodstock_options );

		}
	}
	add_action('wp_enqueue_scripts', 'woodstock_register_js');


endif;


// Megamenu registration

function woodstock_admin_scripts( $hook ) {

	if ( is_admin() ) {

    	wp_enqueue_style("wstock-admin-styles", get_template_directory_uri() . "/css/wp-admin-custom.css", false, "1.0", "all");

			if (class_exists('WPBakeryVisualComposerAbstract')) {
				wp_enqueue_style('wstock-visual-composer', get_template_directory_uri() .'/css/visual-composer.css', false, "1.0", 'all');
			}
    	}
}
add_action('admin_enqueue_scripts', 'woodstock_admin_scripts');


/******************************************************************************/
/****** Overwrite WooCommerce Widgets *****************************************/
/******************************************************************************/


function woodstock_overwride_woocommerce_widgets() {
	if ( class_exists( 'WC_Widget_Cart' ) ) {
		include_once( 'includes/widgets/woocommerce-cart.php' );
		register_widget( 'TDL_WC_Widget_Cart' );
	}
}
add_action( 'widgets_init', 'woodstock_overwride_woocommerce_widgets', 15 );



/*-----------------------------------------------------------------------------------*/
/*	Sidebars
/*-----------------------------------------------------------------------------------*/

if( ! function_exists( 'woodstock_widgets_init' ) ):
	function woodstock_widgets_init() {

		register_sidebar(array(
			'name' => esc_html__('Sidebar', 'woodstock'),
			'id' => 'sidebar',
			'before_widget' => '<aside class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		));

		register_sidebar(array(
			'name' => esc_html__('Shop Sidebar', 'woodstock'),
			'id' => 'widgets-product-listing',
			'before_widget' => '<aside class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		));

		register_sidebar(array(
			'name' => esc_html__('Product Page Sidebar', 'woodstock'),
			'id' => 'widgets-product-page-listing',
			'before_widget' => '<aside class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		));

		register_sidebar(array(
			'name' => esc_html__('Footer 1', 'woodstock'),
			'id' => 'footer-sidebar-1',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		));

		register_sidebar(array(
			'name' => esc_html__('Footer 2', 'woodstock'),
			'id' => 'footer-sidebar-2',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		));

		register_sidebar(array(
			'name' => esc_html__('Footer 3', 'woodstock'),
			'id' => 'footer-sidebar-3',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		));

		register_sidebar(array(
			'name' => esc_html__('Footer 4', 'woodstock'),
			'id' => 'footer-sidebar-4',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		));

}
endif;
add_action( 'widgets_init', 'woodstock_widgets_init' );

/******************************************************************************/
/****** Set woocommerce images sizes ******************************************/
/******************************************************************************/

if ( ! function_exists('woodstock_woocommerce_image_dimensions') ) :
	function woodstock_woocommerce_image_dimensions() {
		global $pagenow;

		if ( ! isset( $_GET['activated'] ) || $pagenow != 'themes.php' ) {
			return;
		}

	  	$catalog = array(
			'width' 	=> '350',	// px
			'height'	=> '380',	// px
			'crop'		=> 1 		// true
		);

		$single = array(
			'width' 	=> '570',	// px
			'height'	=> '619',	// px
			'crop'		=> 1 		// true
		);

		$thumbnail = array(
			'width' 	=> '80',	// px
			'height'	=> '80',	// px
			'crop'		=> 1 		// false
		);

		// Image sizes
		update_option( 'shop_catalog_image_size', $catalog ); 		// Product category thumbs
		update_option( 'shop_single_image_size', $single ); 		// Single product image
		update_option( 'shop_thumbnail_image_size', $thumbnail ); 	// Image gallery thumbs
	}
	add_action( 'after_switch_theme', 'woodstock_woocommerce_image_dimensions', 1 );
endif;


/******************************************************************************/
/*********************** Woodstock setup **************************************/
/******************************************************************************/


if ( ! function_exists( 'woodstock_setup' ) ) :
function woodstock_setup() {

	global $tdl_options;

	/** Theme support **/
	add_theme_support( 'title-tag' );
	add_theme_support( 'menus' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'quote', 'status', 'video', 'audio' ) );
	add_theme_support( 'woocommerce');

	/** Add Image Sizes **/
	$shop_catalog_image_size = get_option( 'shop_catalog_image_size' );
	$shop_single_image_size = get_option( 'shop_single_image_size' );
	add_image_size('woodstock_product_small_thumbnail', (int)$shop_catalog_image_size['width']/3, (int)$shop_catalog_image_size['height']/3, isset($shop_catalog_image_size['crop']) ? true : false); // made from shop_catalog_image_size
	add_image_size('woodstock_shop_single_small_thumbnail', (int)$shop_single_image_size['width']/3, (int)$shop_single_image_size['height']/3, isset($shop_catalog_image_size['crop']) ? true : false); // made from shop_single_image_size
	add_image_size( 'woodstock_blog-isotope', 620, 500, true );


	/* THUMBNAIL SIZES
	================================================== */
	// set_post_thumbnail_size( 520, 150, true);
	add_image_size( 'woodstock-widget-image', 94, 70, true);
	add_image_size( 'woodstock-thumb-square', 120, 120, true);
	add_image_size( 'woodstock-thumb-image', 600, 450, true);
	add_image_size( 'woodstock-thumb-image-twocol', 900, 675, true);
	add_image_size( 'woodstock-thumb-image-onecol', 1800, 1200, true);
	add_image_size( 'woodstock-blog-image', 1280, '', true);
	add_image_size( 'woodstock-gallery-image', 1000, 9999);
	add_image_size( 'woodstock-large-square', 1200, 1200, true);
	add_image_size( 'woodstock-full-width-image-gallery', 1280, 720, true);

	/** Theme textdomain **/
	load_theme_textdomain( 'woodstock', get_template_directory() . '/languages' );


}
endif; // woodstock_setup
add_action( 'after_setup_theme', 'woodstock_setup' );
add_filter( 'auto_update_plugin', '__return_false' );
add_filter( 'auto_update_theme', '__return_false' );


 /********************************************************************************/

 // Add the code below to your theme's functions.php file to add a confirm password field on the register form under My Accounts.
// Add the code below to your theme's functions.php file to add a confirm password field on the register form under My Accounts.
add_filter('woocommerce_registration_errors', 'registration_errors_validation', 10,3);
function registration_errors_validation($reg_errors, $sanitized_user_login, $user_email) {
	global $woocommerce;
	extract( $_POST );
	if ( strcmp( $password, $password2 ) !== 0 ) {
		return new WP_Error( 'registration-error', __( 'Passwords do not match.', 'woocommerce' ) );
	}
	return $reg_errors;
}
add_action( 'woocommerce_register_form', 'wc_register_form_password_repeat' );
function wc_register_form_password_repeat() {
	?>
	<p class="form-row form-row-wide">
		<label for="reg_password2"><?php _e( 'Confirm Password', 'woocommerce' ); ?> <span class="required">*</span></label>
		<input type="password" class="input-text" name="password2" id="reg_password2" value="<?php if ( ! empty( $_POST['password2'] ) ) echo esc_attr( $_POST['password2'] ); ?>" placeholder="Confirm Password"/>
	</p>
	<?php
}


add_filter( 'wp_image_editors', 'change_graphic_lib' );

function change_graphic_lib($array) {
  return array( 'WP_Image_Editor_GD', 'WP_Image_Editor_Imagick' );
}


 function custom_pre_get_posts_query( $q ) {

    $tax_query = (array) $q->get( 'tax_query' );

    $tax_query[] = array(
           'taxonomy' => 'product_cat',
           'field' => 'slug',
           'terms' => array( 'appliances-electronics' ), // Don't display products in the clothing category on the shop page.
           'operator' => 'NOT IN'
    );


    $q->set( 'tax_query', $tax_query );

}
add_action( 'woocommerce_product_query', 'custom_pre_get_posts_query' );


add_action( 'woocommerce_cart_calculate_fees','custom_tax_surcharge_for_swiss', 10, 1 );
function custom_tax_surcharge_for_swiss( $cart ) {

    //if ( is_admin() && ! defined('DOING_AJAX') ) return;

    $user=wp_get_current_user();

    if($user->ID!='70' && $user->ID!='67' && $user->ID!='100' && $user->ID!='38' && $user->ID!='3' && $user->ID!='75'){
        if(in_array('agent',$user->roles)){
       $surcharge = 5;
       $cart->add_fee( __( 'Agent Fee ', 'woocommerce'), $surcharge, false );

       }
    }
}

 function wpb_sender_email( $original_email_address ) {
    return 'jetlitransfer@jetlitransfer.com';
}

// Function to change sender name
function wpb_sender_name( $original_email_from ) {
    return 'JETLI TRANSFER';
}

// Hooking up our functions to WordPress filters
add_filter( 'wp_mail_from', 'wpb_sender_email' );
add_filter( 'wp_mail_from_name', 'wpb_sender_name' );

/*function remove_css_js_ver( $src ) {
if( strpos( $src, '?ver=' ) )
$src = remove_query_arg( 'ver', $src );
return $src;
}
add_filter( 'style_loader_src', 'remove_css_js_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_css_js_ver', 10, 2 );*/

function shopp_disable_autocomplete ($result,$options,$Object) {
    $return = str_replace("<input","<input autocomplete=\"off\"",$result);
    return $return;
}

/**
 * Cycles through an array of input fields names and applies the shopp_disable_autocomplete function to them.
 **/
function shopp_disable_autocomplete_init () {
    $fields = array(
        'firstname','lastname','company','phone','email',
        'billingname','billingaddress','billingxaddress','billingcity','billingpostcode',
        'shippingname','shippingaddress','shippingxaddress','shippingcity','shippingpostcode'
    );
    foreach ( $fields as $field ) {
        add_filter('shopp_themeapi_checkout_'.$field, 'shopp_disable_autocomplete', 11, 3);
    }
}

add_action('shopp_init_checkout','shopp_disable_autocomplete_init');

function my_custom_show_free_label_with_free_shipping( $label, $method ) {
//	if ( $method->method_id == 'local_pickup' ) return $label; // Don't apply to local pickup rates
	if ( $method->cost == 0 ) {
		$label .= '<span class="text-custom">$0.00 <br><p>Free Delivery</p></span>';
	}

		else if ( $method->cost == 10 ) {
		$label .= ' <span class="text-custom">$10.00 <br><p>Delivery in the provinces is 3-5 Business days</p></span>';
	}
	return $label;
}





add_filter( 'woocommerce_cart_shipping_method_full_label', 'my_custom_show_free_label_with_free_shipping', 10, 2 );

add_filter('woocommerce_checkout_get_value','__return_empty_string', 1, 1);


add_filter ('add_to_cart_redirect', 'redirect_to_cart');
function redirect_to_checkout() {
        global $woocommerce;
        //Get product ID
        $product_id = (int) apply_filters('woocommerce_add_to_cart_product_id', $_POST['product_id']);
        //Check if product ID is in a certain taxonomy
        if( has_term( 'membership-donations', 'product_cat', $product_id ) ){
        //Get cart URL
          $checkout_url = get_permalink(get_option('woocommerce_cart_page_id'));
        //Return the new URL
          return $checkout_url;
         };
}
//Proceed To Checkout//
// add_action('woocommerce_before_add_to_cart_form','proceed_to_checkout');

//  function proceed_to_checkout(){
//   global $woocommerce;
//   $checkout_url = $woocommerce->cart->get_checkout_url();

//     echo '<a href="'. $checkout_url .'" class="button">Proceed to Checkout</a>';
//  }

 //Proceed to continue shopping//
//  add_action( 'woocommerce_single_product_summary', 'bbloomer_continue_shopping_button', 31 );

// function bbloomer_continue_shopping_button() {
//   if ( wp_get_referer() ) echo '<a class="button continue" href="' . wp_get_referer() . '">Continue Shopping</a>';
// }

// add_filter( 'wc_add_to_cart_message_html', 'add_continue_shopping_button', 10, 2);
// function add_continue_shopping_button( $message, $products ){
// 		$message .= sprintf( '<br/><a href="%s" class="button wc-forward" style="clear:both;margin-top:5px;">%s</a>', esc_url( wc_get_page_permalink( 'shop' ) ), esc_html__( 'Continue Shopping', 'woocommerce' ) );
// 	return $message;
// }

add_filter( 'woocommerce_shipping_package_name', 'custom_shipping_package_name' );
function custom_shipping_package_name( $name ) {
  return 'Delivery fee for Provinces';
}

function ace_hide_shipping_title( $label ) {
	$pos = strpos( $label, ': ' );
	return substr( $label, ++$pos );
}
add_filter( 'woocommerce_cart_shipping_method_full_label', 'ace_hide_shipping_title' );

add_filter( 'woocommerce_shipping_calculator_enable_city', '__return_true' );
//Redirect to checkout page after registration//

function iconic_register_redirect( $redirect ) {

    return wc_get_page_permalink( 'checkout' );
}

add_filter( 'woocommerce_registration_redirect', 'iconic_register_redirect' );

// Limit Woocommerce phone field to 10 digits number
add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process');

function my_custom_checkout_field_process() {
    global $woocommerce;
    // echo '<pre>';
    // print_r($_POST);
    // Check if set, if its not set add an error. This one is only requite for companies
    if ( ! (preg_match('/^[0-9,+]{12}$/D', $_POST['shipping_phone'] ))){
        wc_add_notice( "Incorrect Recipient Phone Number! Please enter valid 8 digits phone number"  ,'error' );
    }
}
